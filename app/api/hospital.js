/**
 * Descrição: USUARIO
 * Author: Thiago Silva
 */

/*IMPORTANDO BIBLIOTECAS*/
const express = require("express");
const router = express.Router();
const Service = require("../services/hospital");
const doctorService = require("../services/doctor");
const campaignService = require("../services/campaign");
const quizService = require("../services/quiz");
const message = require("../core/message");
const auth = require("../core/auth");
const auth_doctor = require("../core/auth.doctor");
const mailer = require("../core/email");
const templates = require("../core/templates");
const settings = require("../core/settings");

const postos = [
    {
        "state": "AL",
        "name": "Hospital Universitário Prof. Alberto Antunes - UFAL",
        "phone": "(82) 3202-3800",
        "email": "luizaafernandes@hotmail.com",
        "place": "Ambulatório de Dermatologia - Hospital Universitário Prof. Alberto Antunes",
        "district": "Tabuleiro dos Martins",
        "city": "Maceió",
        "cep": "57072-900",
        "code": "F50B0B"
    },
    {
        "state": "BA",
        "name": "Hospital Aristides Maltez",
        "phone": "(71) 3357-6875",
        "email": "tanianm@terra.com.br",
        "place": "Hospital Aristides Maltez",
        "district": "Brotas",
        "city": "Salvador",
        "cep": "40285-001",
        "code": "59A107"
    },
    {
        "state": "BA",
        "name": "Secretaria Municipal de Saúde - Feira de Santana",
        "phone": "(75) 3612-6600",
        "email": "leonardocesar20@uol.com.br",
        "place": "Secretaria Municipal de Saúde- Feira de Santana- BA",
        "district": "Estação Nova",
        "city": "Feira de Santana",
        "cep": "44027-010",
        "code": "2DE636"
    },
    {
        "state": "BA",
        "name": "Serviço Médico Especializado - Policlínica Municipal de Alagoinhas",
        "phone": "(75) 3421-9424",
        "email": "margarethmaciel@hotmail.com",
        "place": "Policlínica Municipal de Alagoinhas",
        "district": "Pq Floresta",
        "city": "Alagoinhas",
        "cep": 48010110,
        "code": "EF4E43"
    },
    {
        "state": "BA",
        "name": "Centro de Saúde DST/AIDS",
        "phone": "(74) 3541-8276",
        "email": "estherv@uol.com.br/ dermatoventin@gmail.com",
        "place": "Centro de Saúde DST/AIDS",
        "district": "Centro",
        "city": "Vitória da Conquista",
        "cep": "45051-000",
        "code": "D755B5"
    },
    {
        "state": "BA",
        "name": "Santo Antônio de Jesus- Policlinica Municipal",
        "phone": "(75) 99850-1172",
        "email": "linemsoares@yahoo.com.br",
        "place": "Policilina Municipal/ Praça Padre  Mateus",
        "district": "Centro",
        "city": "Santo Antônio de Jesus",
        "cep": "44571-029",
        "code": "C6D885"
    },
    {
        "state": "BA",
        "name": "HGRS - Hospital Geral Roberto Santos",
        "phone": "(71) 3237-7582",
        "email": "viviboccanera@gmail.com",
        "place": "HGRS - Hospital Geral Roberto Santos",
        "district": "Cabula",
        "city": "Salvador",
        "cep": "41180-780",
        "code": "F5180B"
    },
    {
        "state": "BA",
        "name": "Universidade Federal do Vale do São Francisco - Liga Acadêmica de Dermatologia e IST",
        "phone": "(75) 3282-3464",
        "email": "isnaiaf@hotmail.com",
        "place": "Centro de Especialidades Médicas Municipal",
        "district": "Centro",
        "city": "Paulo Afonso",
        "cep": "48601-320",
        "code": "A30BA4"
    },
    {
        "state": "BA",
        "name": "USB Almerinda Lomanto",
        "phone": "(73)3526-8423",
        "email": "keurocha@hotmail.com",
        "place": "USB Almerinda Lomanto",
        "district": "Joaquim Romão",
        "city": "Jequié",
        "cep": "45200-310",
        "code": "3BB6B3"
    },
    {
        "state": "CE",
        "name": "Centro de Dermatologia Dona Libânia",
        "phone": "(85) 3101-5452",
        "email": "draclarissedermatologista@gmail.com",
        "place": "Centro de Dermatologia Dona Libânia",
        "district": "Centro",
        "city": "Fortaleza",
        "cep": "60035-101",
        "code": "5CA676"
    },
    {
        "state": "CE",
        "name": "Centro de Dermatologia e Infectologia de Juazeiro do Norte Ceará",
        "phone": "(88) 3566-1055",
        "email": "ccdg27@hotmail.com",
        "place": "Centro de Dermatologia e Infectologia de Juazeiro do Norte",
        "district": "Santa Tereza",
        "city": "Juazeiro do Norte",
        "cep": "63050-400",
        "code": "2E4B0E"
    },
    {
        "state": "CE",
        "name": "Hospital Universitário Walter Cantídio",
        "phone": "(85) 3366-8105",
        "email": "josewilsonaccioly@yahoo.com.br",
        "place": "Hospital Universitário Walter Cantídio",
        "district": "Rodolfo Theófilo",
        "city": "Fortaleza",
        "cep": "60430-370",
        "code": "DAF5F5"
    },
    {
        "state": "DF",
        "name": "Hospital Regional Asa Norte -HRAN",
        "phone": "61 2017-1900",
        "email": "bmdermato@gmail.com,bmdermato@gmail.com",
        "place": "Hospital Regional Asa Norte -HRAN",
        "district": "Asa Norte",
        "city": "Brasília",
        "cep": "71500-000",
        "code": "4149D7"
    },
    {
        "state": "ES",
        "name": "Serviço de Deramtologia do Hospital da Santa Casa de Misericórdia de Vitória",
        "phone": "(27) 3212-7239/ 3212-7200",
        "email": "jbasiliosouza@gmail.com",
        "place": "Igreja Presbiteriana do IBES - posto do Serviço de Deramtologia do Hospital da Santa Casa de Misericórdia de Vitória",
        "district": "IBES",
        "city": "Vila Velha",
        "cep": "29108-630",
        "code": "F19AE2"
    },
    {
        "state": "ES",
        "name": "Policlínica Municipal de Guaçui",
        "phone": "(28) 3553-2869",
        "email": "drwanderley@hotmail.com",
        "place": "Policlínica Municipal de Guaçui",
        "district": "Quincas Machado",
        "city": "Guaçui",
        "cep": "29560-000",
        "code": "94051B"
    },
    {
        "state": "FL",
        "name": "Hospital Universitário Antônio Pedro",
        "phone": "(21) 2629-9000",
        "email": "mgavazzoni@gmail.com",
        "place": "Ambulatório de Dermatologia Hospital Universitário Antônio Pedro",
        "district": "Centro",
        "city": "Niterói",
        "cep": "24033-900",
        "code": "3D183A"
    },
    {
        "state": "FL",
        "name": "Hospital Escola Álvaro Alvim",
        "phone": "(22) 2726-6700",
        "email": "medpaula@yahoo.com.br",
        "place": "Hospital Escola Álvaro Alvim",
        "district": "Centro",
        "city": "Campos dos Goytacazes",
        "cep": "28035-211",
        "code": "E28DA1"
    },
    {
        "state": "FL",
        "name": "Policlinica Centro Dr. Silvio Henrique Braune",
        "phone": "(22) 2521-1687",
        "email": "gfsolange@gmail.com",
        "place": "Policlinica Centro Dr. Silvio Henrique Braune",
        "district": "Centro",
        "city": "Nova Friburgo",
        "cep": "28625-500",
        "code": "11E1T2"
    },
    {
        "state": "GO",
        "name": "Hospital Municipal de Porangatu",
        "phone": "(62)3251-6826 / 99686-0257",
        "email": "ammacedo@terra.com.br",
        "place": "Hospital Municipal de Porangatu",
        "district": "Santa Luzia",
        "city": "Porangatu",
        "cep": "76550-000",
        "code": "637D47"
    },
    {
        "state": "GO",
        "name": "Serviço de Dermatologia do Hospital das Clinicas da Universidade Federal de Goiás",
        "phone": "(62) 3251-6826",
        "email": "marcochaul@hotmail.com",
        "place": "Serviço de Dermatologia do HC/ UFC",
        "district": "Setor Universitário",
        "city": "Goiânia",
        "cep": "74605-050",
        "code": "EF1EA2"
    },
    {
        "state": "MA",
        "name": "Hospital de Referencia Estadual de Alta Complexidade  Dr. Carlos Macieira",
        "phone": "(98) 3190-9091",
        "email": "yanaleda@gmail.com",
        "place": "Hospital de Referência Estadual de Alta Complexidade  Dr. Carlos Macieira",
        "district": "Calhau",
        "city": "São Luís",
        "cep": "65070-220",
        "code": "46D012"
    },
    {
        "state": "MA",
        "name": "Associação Médica de Imperatriz",
        "phone": "(99)3524-3716",
        "email": "dralouiseleal@gmail.com,dralouiseleal@gmail.com",
        "place": "Associação Médica de Imperatriz",
        "district": "Centro",
        "city": "Imperatriz",
        "cep": "65900-330",
        "code": "11A617"
    },
    {
        "state": "MG",
        "name": "Centro Ambulatorial Dr. Agostinho Paolucci ( Hospital Escola)",
        "phone": "(32) 3333-4121",
        "email": "gabrieladecastros@gmail.com",
        "place": "Faculdade de Medicina de Barbacena - Praça Presidente Antonio Carlos",
        "district": "São Sebastiao",
        "city": "Barbacena",
        "cep": "36202-336",
        "code": "F2B99C"
    },
    {
        "state": "MG",
        "name": "Faculdade de Medicina Suprema de Juiz de Fora- MG",
        "phone": "(32) 98843-2731",
        "email": "aloisiogamonal@terra.com.br",
        "place": "Hospital Universitário e Maternidade Therezinha de Jesus - Recepção do primeiro andar",
        "district": "São mateus",
        "city": "Juiz de Fora",
        "cep": "36036-000",
        "code": "0944CB"
    },
    {
        "state": "MG",
        "name": "Serviço de dermatologia da Universidade Federal do Triangulo Mineiro",
        "phone": "(34) 38025440",
        "email": "meireataide@mednet.com.br",
        "place": "Ambulatório de Especialidades da UFTM-Antiga Funepu",
        "district": "Abadia",
        "city": "Uberaba",
        "cep": "38025-440",
        "code": "39348C"
    },
    {
        "state": "MG",
        "name": "Hospital Imaculada Conceição",
        "phone": "(38)3729-1211",
        "email": "tatilamounier23@yahoo.com.br",
        "place": "Hospital Imaculada Conceição",
        "district": "Tibira",
        "city": "Curvelo",
        "cep": "35790-000",
        "code": "D617C1"
    },
    {
        "state": "MG",
        "name": "Serviço de Dermatologia do Hospital Universitario da UF JF",
        "phone": "(32) 3231-2731",
        "email": "shirleygamonal@terra.com.br",
        "place": "HU-UF-JF- UNIDADE BOM BOSCO/ PRIMEIRO ANDAR- RECEPÇÃO- TODAS AS 23 SALAS",
        "district": "Dom Bosco",
        "city": "Juiz de Fora",
        "cep": "36038-330",
        "code": "7026FD"
    },
    {
        "state": "MG",
        "name": "Ambulatorio Municipal Dr. Plínio  Prado Coutinho",
        "phone": "(35)3698-1751",
        "email": "claudiolelis@hotmail.com",
        "place": "Ambulatório  Dr. Plínio  Prado Coutinho",
        "district": "Centro",
        "city": "Alfenas",
        "cep": "37130-000",
        "code": "516A48"
    },
    {
        "state": "MG",
        "name": "Unidade de Atendimento Especializando em Saúde (UAES)",
        "phone": "31-3612-5580",
        "email": "virginia.vinha@yahoo.com.br",
        "place": "Unidade de Atendimento Especializando em Saúde (UAES)",
        "district": "Centro",
        "city": "Viçosa",
        "cep": "36570-133",
        "code": "9BF236"
    },
    {
        "state": "MG",
        "name": "Policlínica Dr. Ovídio Nogueira Machado",
        "phone": "(37) 3242-3693",
        "email": "mtuliodermato@gmail.com",
        "place": "Ambulatório de Dermatologia da Policlínica Dr. Ovídio Nogueira Machado",
        "district": "Niterói",
        "city": "Itaúna",
        "cep": "35680-403",
        "code": "59A8E8"
    },
    {
        "state": "MG",
        "name": "Associação de Amparo a pacientes com Câncer",
        "phone": "(32) 3379-4117/98823-3073",
        "email": "mahyraribeiro@hotmail.com",
        "place": "Associação de Amparo a pacientes com Câncer",
        "district": "Centro",
        "city": "São João Del Rei",
        "cep": "36307-330",
        "code": "4AE138"
    },
    {
        "state": "MG",
        "name": "Faculdade de Medicina de Itajubá - MG",
        "phone": "(35) 3629-7800",
        "email": "clarissascr@gmail.com",
        "place": "Faculdade de Medicina de Itajubá",
        "district": "São Vicente",
        "city": "Itajubá",
        "cep": "37502-138",
        "code": "44B1Q2"
    },
    {
        "state": "MS",
        "name": "C.E.M- Centro de Especialidades Garcia, 280",
        "phone": "(67) 3324-2511",
        "email": "mariaangelicagorga@clinicacorporetl.com.br",
        "place": "C.E.M- Centro de Especialidades Garcia, 280",
        "district": "Santos Dumont",
        "city": "Três Lagoas",
        "cep": "79600-971",
        "code": "290C0A"
    },
    {
        "state": "MS",
        "name": "Hospital Universitário (Serviço de Dermatologia Dr. Gunter Hans) HUMAP/UFMS",
        "phone": "(67)3345-3226",
        "email": "lidianeoc@yahoo.com.br",
        "place": "Hospital Universitário Serviço de Dermatologia Dr. Gunter Hans",
        "district": "Vila  Ipiranga",
        "city": "Campo Grande",
        "cep": "79080-190",
        "code": "41B237"
    },
    {
        "state": "MT",
        "name": "CEADAS - Centro de Especialidades e Apoio Diagnóstico Albert Sabin",
        "phone": "(66) 3411-4354",
        "email": "hccaires@gmail.com",
        "place": "CEADAS - Centro de Especialidades e Apoio Diagnóstico Albert Sabin",
        "district": "Vila Adriana",
        "city": "Rondonópolis",
        "cep": "78710-445",
        "code": "1CF061"
    },
    {
        "state": "MT",
        "name": "Hospital Universitário Julio Muller",
        "phone": "(65) 3615-7281",
        "email": "paulopacola@bol.com.br",
        "place": "Hospital Universitário Julio Muller - Ambulatório 3",
        "district": "Alvorada",
        "city": "Cuiabá",
        "cep": "78048-902",
        "code": "7B2ED6"
    },
    {
        "state": "MT",
        "name": "Unidade de Saúde da Familia Centro",
        "phone": "(65) 3311-4800",
        "email": "marcelameissner@gmail.com",
        "place": "Unidade de Saúde da Família Centro",
        "district": "Centro",
        "city": "Tangará da Serra",
        "cep": "78300-000",
        "code": "F6BA55"
    },
    {
        "state": "PA",
        "name": "Universidade do Estado do Pará",
        "phone": "não informado",
        "email": "reginacarneiro@globo.com",
        "place": "Ambulatório de Dermatologia da Universidade do Estado do Pará -UEPA",
        "district": "Marco",
        "city": "Belém",
        "cep": "66095-662",
        "code": "15E1T2"
    },
    {
        "state": "PA",
        "name": "Serviço de Dermatologia da Universidade Federal do Pará/ HUJBB",
        "phone": "(91)3201-6731",
        "email": "deborahunger@hotmail.com",
        "place": "Ambulatório da Dermatologia da Universidade Federal do Pará/ HUJBB",
        "district": "Guamá",
        "city": "Belém",
        "cep": "66073-700",
        "code": "2EBFBB"
    },
    {
        "state": "PE",
        "name": "POLICLINICA UNIVASF",
        "phone": "não informado",
        "email": "paulyanesampaio@yahoo.com.br",
        "place": "POLICLINICA UNIVASF",
        "district": "Maria Auxiliadora",
        "city": "Petrolina",
        "cep": "56330-460",
        "code": "B7BDBC"
    },
    {
        "state": "PE",
        "name": "Policlínica Farmaceutica Narciso Lima-Hospital Público de Bezerro",
        "phone": "(81) 3728-6726",
        "email": "franciscodermatologia@gmail.com",
        "place": "Policlínica Farmaceutica Narciso Lima-Hospital Público de Bezerro",
        "district": "Santo Amaro",
        "city": "Bezerro",
        "cep": "55660-000",
        "code": "909EF7"
    },
    {
        "state": "PE",
        "name": "IMIP - Escola Pernambucana de Saúde",
        "phone": "(81) 2122-4100",
        "email": "deborahcastro.nid@gmail.com",
        "place": "Escola Pernambucana de Saúde - IMIP",
        "district": "Boa Vista",
        "city": "Recife",
        "cep": "50070-550",
        "code": "5BDAC9"
    },
    {
        "state": "PE",
        "name": "Hospital Universitário Oswaldo Cruz - Universidade de Pernambuco",
        "phone": "(81) 3413-1300 / 3413-1302",
        "email": "aldejane.gurgel@hotmail.com",
        "place": "Hospital Universitário Oswaldo Cruz - Universidade de Pernambuco",
        "district": "Boa Vista",
        "city": "Recife",
        "cep": "50100-130",
        "code": "7DE77C"
    },
    {
        "state": "PE",
        "name": "Hospital Santo Amaro - Santa Casa de Misericórdia - CEDER",
        "phone": "(81) 3421-5766",
        "email": "lucasdpacheco@gmail.com",
        "place": "Hospital Santo Amaro - Santa Casa de Misericórdia - CEDER",
        "district": "Santo Amaro",
        "city": "Recife",
        "cep": "50040-000",
        "code": "7C4A8D"
    },
    {
        "state": "PE",
        "name": "Hospital das Clínicas - Universidade Federal de Pernambuco",
        "phone": "(81) 2126-3528 / 2126-8561",
        "email": "claudiaelise@yahoo.com.br",
        "place": "Hospital das Clínicas - Universidade Federal de Pernambuco",
        "district": "Em frente a Sudene da Cidade Universitária",
        "city": "Recife",
        "cep": "50570-420",
        "code": "25B162"
    },
    {
        "state": "PE",
        "name": "Hospital Otávio de Freitas",
        "phone": "(81) 3182-8500",
        "email": "katiaderma@hotmail.com",
        "place": "Hospital Otávio de Freitas",
        "district": "Tejipió",
        "city": "Recife",
        "cep": "50920-850",
        "code": "AC2E39"
    },
    {
        "state": "PE",
        "name": "UPAE - Serra Talhada - PE",
        "phone": "87-3831-3761",
        "email": "cecilia_leite@hotmail.com",
        "place": "UPAE - Serra Talhada",
        "district": "Cagepe",
        "city": "Serra Talhada",
        "cep": "56909-680",
        "code": "5F6813"
    },
    {
        "state": "PE",
        "name": "UPAE - CARUARÚ - PE",
        "phone": "(81) 3725-7549",
        "email": "leila_bele@hotmail.com",
        "place": "UPAE - CARUARÚ - PE",
        "district": "Indianópolis",
        "city": "Caruarú",
        "cep": "55026-530",
        "code": "1D235E"
    },
    {
        "state": "PB",
        "name": "Hosp. Universitário Lauro Wanderley",
        "phone": "(83)3216-7042",
        "email": "azzouz@globo.com",
        "place": "Ambulatório de Dermatologia do Hospital Lauro Wanderley",
        "district": "Castelo Branco",
        "city": "João Pessoa",
        "cep": "58050-000",
        "code": "52EE92"
    },
    {
        "state": "PB",
        "name": "Hospital Alcides Carneiro",
        "phone": "(83) 3341-1616",
        "email": "lucianasrabello@uol.com.br,fvieiropolis@uol.com.br",
        "place": "Hospital Universitario Alcides Carneiro",
        "district": "São José",
        "city": "Campina Grande",
        "cep": "paraiba",
        "code": "91D07A"
    },
    {
        "state": "PB",
        "name": "Serviço de Residencia e Especialização em Dermatologia da Faculdade de Medicina Nova Esperança (FAMENE)",
        "phone": "(82) 2106-4770, 2106-4788 (COREME)",
        "email": "trindadeluc@gmail.com",
        "place": "Centro de Saúde Nova Esperança-Unidade II (Valentina)",
        "district": "Gramame",
        "city": "Joao Pessoa",
        "cep": "58067-698",
        "code": "01244D"
    },
    {
        "state": "PI",
        "name": "Hospital Getúlio Vargas",
        "phone": "(86) 3221-3040 - Ramal 157",
        "email": "sheilacastelo@hotmail.com",
        "place": "Hospital Getúlio Vargas - Ambulatório de Dermatologia",
        "district": "Centro",
        "city": "Teresina",
        "cep": "64001-020",
        "code": "22C2R2"
    },
    {
        "state": "PI",
        "name": "Ambulatório do Hospital e Maternidade Marques Bastos",
        "phone": "(86) 3221-3040 - Ramal 157",
        "email": "draludmilladermato@gmail.com",
        "place": "Ambulatório do Hospital e Maternidade Marques Bastos",
        "district": "Centro",
        "city": "Parnaíba",
        "cep": "64200-280",
        "code": "815C88"
    },
    {
        "state": "PR",
        "name": "Serviço de Dermatologia do Complexo Hospital de Clínicas Universidade Federal do PR",
        "phone": "(41) 3360-1800 / 3360-7899",
        "email": "dra.fabiane@cepelle.com.br/ fmbrenner@ufpr.br",
        "place": "Ambulatório de Dermatologia SAM 4 anexo B prédio central",
        "district": "Centro",
        "city": "Curitiba",
        "cep": "80060-900",
        "code": "5B04E7"
    },
    {
        "state": "PR",
        "name": "Autarquia Municipal de Saúde",
        "phone": "(43) 3422-1090",
        "email": "alfredobacciotti@hotmail.com/flaviacrisca2019@gmail.com",
        "place": "Autarquia Municipal de Saúde",
        "district": "Centro",
        "city": "Apucarana",
        "cep": "86800-260",
        "code": "18109F"
    },
    {
        "state": "PR",
        "name": "Hospital Universitário  Oeste do Paraná",
        "phone": "(45) 3321-4663",
        "email": "danibonatto@gmail.com",
        "place": "Ambulatório do Hospital Universitário da Universidade Estadual do Paraná (HUOP)",
        "district": "Santo Onofre",
        "city": "Cascavel",
        "cep": "85806-470",
        "code": "54DB51"
    },
    {
        "state": "PR",
        "name": "Serviço de Dermatologia do Hospital Universitario Regional do Norte do Paraná/Universidade Estadual de Londrina",
        "phone": "(43) 3371-2278",
        "email": "airton@sercomtel.com.br",
        "place": "Ambulatório de Especialidade do Hospital Universitario- AEHU- Piso Térreo",
        "district": "Campus Universitário",
        "city": "Londrina",
        "cep": "86057-970",
        "code": "8BD5FB"
    },
    {
        "state": "PR",
        "name": "CRE -Centro Regional de Especialidade",
        "phone": "(44) 3423-9700",
        "email": "gatomancha@uol.com.br, nataliavassolerenfermeira@outlook.com",
        "place": "CRE - Centro Regional Especialidade",
        "district": "Centro",
        "city": "Paranavai",
        "cep": "87703-320",
        "code": "E0797C"
    },
    {
        "state": "PR",
        "name": "Hospital Municipal Padre Germano Lauck",
        "phone": "(45) 3521-1750",
        "email": "andre.dermato@terra.com.br",
        "place": "Hospital Municipal Pe. Germano Lauck (ambulatório)",
        "district": "Parque Monjolos",
        "city": "Foz do Iguaçu",
        "cep": "85864-380",
        "code": "50C536"
    },
    {
        "state": "PR",
        "name": "Central de Especialidades da Prefeitura do Município de Toledo.",
        "phone": "(45) 3252-6236",
        "email": "fabiwelter@hotmail.com",
        "place": "Unimed Costa Oeste",
        "district": "Centro",
        "city": "Toledo",
        "cep": "85900-010",
        "code": "7A45C3"
    },
    {
        "state": "RJ",
        "name": "Hospital Universitário Gafre e Guinle",
        "phone": "(21) 2264-1114",
        "email": "paulorobertoissa@gmail.com",
        "place": "Ambulatório de Dermatologia do  Hospital Universitário Gafre e Guinle",
        "district": "Tijuca",
        "city": "Rio de Janeiro",
        "cep": "20270-004",
        "code": "C4CFFD"
    },
    {
        "state": "RJ",
        "name": "Policlínica Geral do Rio de Janeiro",
        "phone": "(21)2544-1068",
        "email": "fabriciolamy@hotmail.com |curttreu@gmail.com",
        "place": "Serviço de Dermarologia da Policlinica Geral do RJ",
        "district": "Castelo",
        "city": "Rio de Janeiro",
        "cep": "20020-100",
        "code": "D4C646"
    },
    {
        "state": "RJ",
        "name": "Serviço de Dermatologia do HU  Fraga Filho - UFRJ",
        "phone": "(21) 3928-2589",
        "email": "dermatologia@hucff.ufrj.br",
        "place": "Ambulatório de Cirurgia Dermatologica do Serviço de Dermatologia do HU  Fraga Filho",
        "district": "Ilha do Fundão",
        "city": "Rio de Janeiro",
        "cep": "21941-913",
        "code": "33CRE7"
    },
    {
        "state": "RJ",
        "name": "Serviço de dermatologia do Hospital Universitário Pedro Ernesto - / Universidade do Estado do Rio de Janeiro",
        "phone": "(21)2868-8478",
        "email": "cbbarcaui@gmail.com",
        "place": "Centro Universitário de Controle do Câncer - CUCC- térreo- Hospital Universitario Pedro Ernesto",
        "district": "Vila Isabel",
        "city": "Rio de Janeiro",
        "cep": "20551-030",
        "code": "WECDD7"
    },
    {
        "state": "RJ",
        "name": "Serviço De dermatologia Tropical do Hospital Central do Exército",
        "phone": "(21) 3891-7035",
        "email": "celmilhomens@gmail.com",
        "place": "Emergencia do Hospital Central do Exército",
        "district": "Benfica",
        "city": "Rio de Janeiro",
        "cep": "20911-270",
        "code": "D30A85"
    },
    {
        "state": "RJ",
        "name": "Instituto de Dermatologia Professor Rubem David Azulay",
        "phone": "(21) 2220-1928",
        "email": "joaquimesquitafilho@hotmail.com/ mabenez@gmail.com",
        "place": "Ambulatório do Instituto de Dermatologia Professor Rubem David Azulay - segundo andar do Pavilhão São Miuel",
        "district": "Castelo (centro)",
        "city": "Rio de Janeiro",
        "cep": "20020-022",
        "code": "A44A93"
    },
    {
        "state": "RJ",
        "name": "Hospital  Federal dos Servidores do Estado",
        "phone": "(21) 2291-3131",
        "email": "paulooldani@globo.com",
        "place": "Hospital  Federal dos Servidores do Estado - Prédios dos ambulatórios",
        "district": "Saúde",
        "city": "Rio de Janeiro",
        "cep": "20221-903",
        "code": "E87934"
    },
    {
        "state": "RJ",
        "name": "Serviço de Dermatologia do Hospital  Federal  de Bonsucesso",
        "phone": "(21) 3977-9762",
        "email": "paulagurfinkel@gmail.com",
        "place": "Ambulatório do Hospital Federal de Bonsucesso, prédio 6, andar térreo",
        "district": "Bonsucesso",
        "city": "Rio de Janeiro",
        "cep": "21041-030",
        "code": "EC222Q"
    },
    {
        "state": "RJ",
        "name": "Hospital Central Aristacho Pessoa",
        "phone": "(21)2333-7753 ( Direção)/ 2333-7741 (Ambulatorio)",
        "email": "sandra_martello@hotmail.com",
        "place": "Ambulatório de Dermatologia do Hospital Central Aristacho Pessoa",
        "district": "Rio Comprido",
        "city": "Rio de Janeiro",
        "cep": "20261-243",
        "code": "1353CC"
    },
    {
        "state": "RJ",
        "name": "Hospital Federal  da Lagoa",
        "phone": "(21)3111-5259",
        "email": "avelleira@uol.com.br",
        "place": "Polo de Inclusão Social Padre Velloso - Posto Hospital Federal  da Lagoa",
        "district": "Botafogo",
        "city": "Rio de Janeiro",
        "cep": "22260-130",
        "code": "1274F0"
    },
    {
        "state": "RN",
        "name": "Hospital Dr. Luiz Antonio - Liga Contra o Câncer",
        "phone": "(84) 4009-5401",
        "email": "reginadjales@hotmail.com",
        "place": "CECAN - Centro Avançado de Oncologia (Liga Norte Riograndense Contra o Câncer)",
        "district": "Dix-Sept Rosado",
        "city": "Natal",
        "cep": "59075-740",
        "code": "FC272D"
    },
    {
        "state": "RN",
        "name": "Hospital de Oncologia do Seridó",
        "phone": "(84) 3421-1585",
        "email": "sbdermato.rn@gmail.com",
        "place": "Hospital de Oncologia do Seridó",
        "district": "Centro",
        "city": "Caicó",
        "cep": "59300-000",
        "code": "929A58"
    },
    {
        "state": "RS",
        "name": "Ambulatório de Dermatologia Sanitária",
        "phone": "(51) 3288-7654",
        "email": "reheck2@yahoo.com.br",
        "place": "Ambulatório de Dermatologia Sanitária",
        "district": "Farroupilha",
        "city": "Porto Alegre",
        "cep": "90040-001",
        "code": "33FYR6"
    },
    {
        "state": "RS",
        "name": "Centro Clínico Univates",
        "phone": "(51) 3714-7000 (ramal: 5746)",
        "email": "doris.shansis@terra.com.br",
        "place": "Ambulatório de Especialidades Médicas do Centro Clínico Univates- Sertor C- Prédio 22",
        "district": "Bairro Carneiros",
        "city": "Lajeado",
        "cep": "95913-528",
        "code": "F164A1"
    },
    {
        "state": "RS",
        "name": "Centro de Especialidades de Pelotas",
        "phone": "(53) 3222-1426",
        "email": "anaelizabomfim@hotmail.com",
        "place": "Centro de Especialidades Médicas de Pelotas",
        "district": "Centro",
        "city": "Pelotas",
        "cep": "96015-730",
        "code": "9B26F9"
    },
    {
        "state": "RS",
        "name": "Residencia de Dermatologia UFFS/HSVP",
        "phone": "(54) 3335-8511",
        "email": "camilaferrondermato@gmail.com",
        "place": "Ambulatório de Dermatologia UFFS/HSVP - Antigo Quartel",
        "district": "Centro",
        "city": "Passo Fundo",
        "cep": "99010-080",
        "code": "072B49"
    },
    {
        "state": "RS",
        "name": "Hospital Universitário  Miguel Riet Correa Jr.",
        "phone": "(53) 3233-8800",
        "email": "dermofabio@gmail.com",
        "place": "Ambulatório do Hospital Universitário Miguel Riet Correa Jr.",
        "district": "Centro",
        "city": "Rio Grande",
        "cep": "96200-190",
        "code": "C8902B"
    },
    {
        "state": "RS",
        "name": "Hospital Ana Nery",
        "phone": "(51) 2106-4400",
        "email": "taniazuquetto@hotmail.com",
        "place": "Hospital Ana Nery",
        "district": "Ana Nery",
        "city": "Santa Cruz do Sul",
        "cep": "96835-090",
        "code": "85306D"
    },
    {
        "state": "RS",
        "name": "Universidade Federal do Pampa- UNIPAMPA campus uruguaina",
        "phone": "(553911-0200",
        "email": "lilian_mferreira@hotmail.com",
        "place": "Policilina Municipal de Uruguaiana",
        "district": "Centro",
        "city": "Uruguaiana",
        "cep": "97510-430",
        "code": "A55BCF"
    },
    {
        "state": "RS",
        "name": "Posto de Saúde IAPI",
        "phone": "(51)3289-3400",
        "email": "tacianad@terra.com.br",
        "place": "Posto de Saúde IAPI",
        "district": "Passo  D'areia",
        "city": "Porto Alegre",
        "cep": "90520-200",
        "code": "22D4F9"
    },
    {
        "state": "RS",
        "name": "Ambulatório de Dermatologia do Curso de Medicina da Universidade Luterana do Brasil (ULBRA)- Hospital Universitario da ULBRA",
        "phone": "(51) 3477-9171",
        "email": "giancarlobessa@gmail.com",
        "place": "Ambulatório do Hospital Universitário da ULBRA - 3° andar do anexo ao Hospital Universitário",
        "district": "São José",
        "city": "Canoas",
        "cep": "92425-900",
        "code": "88EF1D"
    },
    {
        "state": "RS",
        "name": "Universidade de Caxias do Sul",
        "phone": "(54)3218-2082",
        "email": "mayara_oliveira@hotmail.com/ jthenriq@ucs.br",
        "place": "Centro Clínico da Universidade de Caxias do Sul- CECLIN",
        "district": "Petropolis",
        "city": "Caxias do Sul",
        "cep": "95.070-560",
        "code": "8F22E9"
    },
    {
        "state": "SE",
        "name": "Serviço de Dermatologia do Hospital Universitário DE Sergipe - HU/UFS",
        "phone": "(79) 2105-1700",
        "email": "bsantanadermato@gmail.com",
        "place": "Hospital Universitário de Sergipe",
        "district": "Cidade Nova",
        "city": "Aracaju",
        "cep": "49060-108",
        "code": "A46723"
    },
    {
        "state": "SC",
        "name": "AMU - Ambulatório Multidisciplinar Universitário",
        "phone": "(48) 3279-1275/ 3279-1385",
        "email": "robertodermatologista@yahoo.com.br",
        "place": "AMU - Ambulatório Multidisciplinar Universitário",
        "district": "Centro",
        "city": "Joaçaba",
        "cep": "89600-000",
        "code": "F6EC56"
    },
    {
        "state": "SC",
        "name": "CRESM - Central de Referencia em Saúde Municipal de Chapecó",
        "phone": "(48) 3279-1275/ 3273-1385",
        "email": "clinicadermatologicabet@hotmail.com",
        "place": "CRESM - Central de Referencia em Saúde Municipal de Chapecó",
        "district": "Centro",
        "city": "Chapecó",
        "cep": "89800-000",
        "code": "CEA93F"
    },
    {
        "state": "SC",
        "name": "Ambulatório Médico de Especialidades- Curso de Medicina do Sul de Santa Catarina- Tubarão/SC",
        "phone": "(48) 3279-1275/ 3273-1385",
        "email": "marianecfissmer@gmail.com",
        "place": "Ambulatório Médico de Especialidades- Curso de Medicina do Sul de Santa Catarina- Tubarão/SC",
        "district": "Dehon",
        "city": "Tubarão",
        "cep": "88704-900",
        "code": "B94454"
    },
    {
        "state": "SC",
        "name": "Centro de Saúde do Município de Brusque/ Andar Térreo",
        "phone": "(47) 3255-6830",
        "email": "anakrisss@hotmail.com/ anakrisderm@gmail.com",
        "place": "Centro de Saúde do Município de Brusque/ Andar Térreo",
        "district": "Centro",
        "city": "Brusque",
        "cep": "88350-170",
        "code": "DDC0DD"
    },
    {
        "state": "SC",
        "name": "Ambulatório de Medicina da Universidade do Vale do Itajaí",
        "phone": "(47) 3341-7788",
        "email": "nicolacopulos@hotmail.com,dr.nicolacopulos@gmail.com",
        "place": "Ambulatório de Medicina da Universidade do Vale do Itajaí",
        "district": "Centro",
        "city": "Itajai",
        "cep": "88302-901",
        "code": "E79D8F"
    },
    {
        "state": "SC",
        "name": "Policlínica Municipal de Palhoça - UNISUL- CNES",
        "phone": "(48) 3279-1275/ 3279-1385",
        "email": "marcelorigatti@gmail.com",
        "place": "Policlínica Municipal de Palhoça - UNISUL- CNES",
        "district": "Centro",
        "city": "Palhoça",
        "cep": "88130-220",
        "code": "4AA6B8"
    },
    {
        "state": "SC",
        "name": "CAMPUS 5 FURB( Hospital Universitário e Policlínica Universitário)",
        "phone": "(47) 3702-6530",
        "email": "deborabrosa@gmail.com",
        "place": "CAMPUS 5 FURB( Hospital Universitário e Policlínica Universitário)",
        "district": "Fortaleza Alta",
        "city": "Blumenau",
        "cep": "89058-010",
        "code": "D13716"
    },
    {
        "state": "SC",
        "name": "Policlínica de Especialidades João Biron (Centro Vida)",
        "phone": "(47) 2106-8500",
        "email": "michel.mentges@hotmail.com",
        "place": "Policlínica de Especialidades João Biron (Centro Vida)",
        "district": "Centro",
        "city": "Jaraguá do Sul",
        "cep": "89259-260",
        "code": "709A96"
    },
    {
        "state": "SP",
        "name": "Hospital de Amor de Barretos",
        "phone": "(17) 3321-6600",
        "email": "criskin.bmc@gmail.com",
        "place": "Unidade  Móvel do Hospital de Amor de Barretos",
        "district": "Centro",
        "city": "Barretos",
        "cep": "São Paulo",
        "code": "9FD1A8"
    },
    {
        "state": "SP",
        "name": "Complexo Hospital Padre Bento Guarulhos",
        "phone": "(11) 2463-5650",
        "email": "jrpegas@terra.com.br",
        "place": "Complexo Hospitalar Padre Bento em Guarulhos- Ambulatorio de Dermatologia",
        "district": "Jardim Tranquilidade",
        "city": "Guarulhos",
        "cep": "07051-000",
        "code": "06D410"
    },
    {
        "state": "SP",
        "name": "Serviço de Dermatologia da Faculdade de Medicina de Marília (FAMEMA)",
        "phone": "(14) 3402-1744 ( Ambulatorio)",
        "email": "andreadermato@hotmail.com",
        "place": "Ambulatório do Hospital São Francisco",
        "district": "Monte Castelo",
        "city": "Marília",
        "cep": "17522-230",
        "code": "DEC771"
    },
    {
        "state": "SP",
        "name": "NGA- 2 Núcleo de Gestão Assistencial 2",
        "phone": "(18) 3836-1190",
        "email": "DENISEPOLETTO@UNISALESIANO.COM.BR",
        "place": "NGA- 2 Núcleo de Gestão Assistencial 2",
        "district": "São João",
        "city": "Araçatuba",
        "cep": "16010-640",
        "code": "EC24D9"
    },
    {
        "state": "SP",
        "name": "Residencia Médica em Dermatologia do instituto de Ensino e Pesquisa  do Hospital Sírio e Libanês",
        "phone": "(11) 3394-4771 (Secretaria Academica)",
        "email": "tovoclinic@uol.com.br",
        "place": "Prédio Hospital Sírio Libanês- Ambulatorio CQC- SALA 919/918",
        "district": "Bela Vista",
        "city": "São Paulo",
        "cep": "01308-050",
        "code": "6311DA"
    },
    {
        "state": "SP",
        "name": "Hospital Universitário de Taubaté- HU/ UNITAU",
        "phone": "(12)3625-7533",
        "email": "dermagica@uol.com.br",
        "place": "Hospital Universitário de Taubaté (Ambulatório Geral)",
        "district": "Centro",
        "city": "Taubaté",
        "cep": "12020-130",
        "code": "D1CE8C"
    },
    {
        "state": "SP",
        "name": "Associação Centro de Estudo Dermatologicos de Jundiai",
        "phone": "(11) 4521-1599",
        "email": "celia.dermato@terra.com.br",
        "place": "Ambulatório da Associação Centro de Estudo Dermatologicos de Jundiai",
        "district": "Vila Arens",
        "city": "Jundiaí",
        "cep": "13.202-560",
        "code": "F13573"
    },
    {
        "state": "SP",
        "name": "Serviço Credenciado \"Fundação Lusíada\" - UNILUS",
        "phone": "(13) 99125-6979",
        "email": "sandradinato@yahoo.com.br",
        "place": "Hospital Guilherme Álvaro",
        "district": "Boqueirão",
        "city": "Sim- Se houver urgencia, no dia. Em geral, agnedadas+B108:I108",
        "cep": "11045-101",
        "code": "E5E967"
    },
    {
        "state": "SP",
        "name": "Centro Universitário de Saúde do ABC- FMABC",
        "phone": "(11) 4993-5476/ 4993-5455",
        "email": "crislacz@hotmail.com, dermatologia@fmabc.br",
        "place": "Centro de Especialidade -  Posto do Centro Universitáriode  Saúde do ABC- FMABC",
        "district": "Centro",
        "city": "Santo André",
        "cep": "09010-130",
        "code": "BB81CC"
    },
    {
        "state": "SP",
        "name": "Serviço de dermatologia da Faculdade Estadual de Medicina de São Jose do Rio Preto-SP",
        "phone": "(17) 3201-5000/ RAMAL 5732",
        "email": "dr.joao@terra.com.br",
        "place": "Praça Dom Jose Marcondes - Posto da Faculdade Estadual de Medicina de São Jose do Rio Preto - SP",
        "district": "Centro",
        "city": "Jose do Rio Preto",
        "cep": "15010-045",
        "code": "E2394B"
    },
    {
        "state": "SP",
        "name": "Divisão de Dermatologia do Hospital das Clínicas da FMUSP",
        "phone": "(11) 2661-9484 Rosangela e Amadeu",
        "email": "rosangela.sousa@hc.fm.usp.br / eugenio.p@hc.fm.usp.br",
        "place": "Ambulatório de Dermatologia 5º Andar Bloco 2 B -  Prédio dos Ambulatórios -  Hospital das Clínicas da FMUSP",
        "district": "Cerqueira César",
        "city": "São Paulo",
        "cep": "0503-900",
        "code": "133FFE"
    },
    {
        "state": "SP",
        "name": "Hospital do Servidor Público Estadual- SP",
        "phone": "(11) 4573-8250",
        "email": "dermatologia@iamspe.sp.gov.br",
        "place": "Hospital do Servidor Público Estadual- SP - Ambulatorio do 1 andar",
        "district": "Vila Clementino",
        "city": "São Paulo",
        "cep": "04039-000",
        "code": "CC377E"
    },
    {
        "state": "SP",
        "name": "HSPM-Hospital do Servidor Público Municipal",
        "phone": "(11) 3241-3665 / 3397-8105",
        "email": "niltongioia@terra.com.br",
        "place": "HSPM-Hospital do Servidor Público Municipal",
        "district": "Aclimação",
        "city": "São Paulo",
        "cep": "01532-000",
        "code": "F68862"
    },
    {
        "state": "SP",
        "name": "Serviço de Dermatologia do Hospital Heliópolis",
        "phone": "(11) 2067-0604/ 99123-6880 - falar com Vanessa",
        "email": "domingos-simone@uol.com.br , helioderma@gmail.com",
        "place": "Ambulatório de Dermatologia do Hospital Heliópolis",
        "district": "Cidade Nova Heliópolis",
        "city": "São Paulo",
        "cep": "04231-030",
        "code": "D1E78B"
    },
    {
        "state": "SP",
        "name": "Hospital da PUC de Campinas",
        "phone": "(19)3343-8496/ 3343-8600",
        "email": "andresimiao@gmail.com, dermato@hmcp.puc-campinas.edu.br",
        "place": "Ambulatório de Dermatologia da PUC Campinas - Corredor Cinza",
        "district": "Jardim Ipaussurama",
        "city": "Campinas",
        "cep": "13059-900",
        "code": "872B6F"
    },
    {
        "state": "SP",
        "name": "Zeferino Vaz UNICAMP",
        "phone": "(19) 3521-7602 , 3521-7776",
        "email": "renatafmagalhaes_dra@hotmail.com",
        "place": "Ambulatório de Especialidades do Hospital Municipal Dr. Mário Gatti -",
        "district": "Parque Itália",
        "city": "Campinas",
        "cep": "13036-902",
        "code": "F22829"
    },
    {
        "state": "SP",
        "name": "Departamento de Dermatologia e Radioterapia -Faculdade de Medicina de Botucatu /UNESP",
        "phone": "(14) 3880-1259/1263/1267",
        "email": "JULIANO.SCHMITT@UNESP.BR",
        "place": "Centro de Saúde Escola - Faculdade de Medicina de Botucatu /UNESP",
        "district": "Vila dos Lavradores",
        "city": "Botucatu",
        "cep": "18609-055",
        "code": "C32B1E"
    },
    {
        "state": "SP",
        "name": "Universidade Federal de São Paulo - UNIFESP",
        "phone": "(11) 5576-8480 (Ramal 1740)",
        "email": "maridiasbatista@gmail.com",
        "place": "Depto de Dermatologia da Universidade Federal de São Paulo - UNIFESP",
        "district": "Vila Clementino",
        "city": "São Paulo",
        "cep": "04038-000",
        "code": "17ACD3"
    },
    {
        "state": "SP",
        "name": "Universidade de Mogi das Cruzes",
        "phone": "(11) 4799-5383",
        "email": "andrederma@hotmail.com/ contato@andrepessanha.com.br",
        "place": "Policlínica /FAEP - Universidade de Mogi das Cruzes",
        "district": "Centro",
        "city": "Mogi das Cruzes",
        "cep": "08780-070",
        "code": "A5E104"
    },
    {
        "state": "SP",
        "name": "Serviço de Dermatologia do Hospital Ipiranga de São Paulo",
        "phone": "(11) 2067-7902 -COREME- Sra. Giane ou Sr. André",
        "email": "cristianohorta@uol.com.br",
        "place": "Ambulatório de Dermatologia - Prédio dos Ambulatórios: saguão de atendimento,  5º Andar - HOSPITAL IPIRANGA",
        "district": "Ipiranga",
        "city": "São Paulo",
        "cep": "04252-000",
        "code": "6C07B5"
    },
    {
        "state": "SP",
        "name": "HOSPITAL REGIONAL PRESIDENTE PRUDENTE/ Unoeste",
        "phone": "(18) 3229-1500",
        "email": "murilolima@gmail.com",
        "place": "AMBULATÓRIO DO HOSPITAL REGIONAL PRESIDENTE PRUDENTE",
        "district": "JD Aquinópolis",
        "city": "Presidente Prudente",
        "cep": "19061-450",
        "code": "0A478E"
    },
    {
        "state": "SP",
        "name": "Casa de Saúde Santa Marcelina",
        "phone": "(11) 2070-6295/ 6291",
        "email": "bebarban@uol.com.br/ adrianabueno@santamarcelina.org (IMPORTANTE ENVIAR AS MENSAGENS NOS DOIS E-MAIL`S)",
        "place": "AME ZONA LESTE- Itaquera",
        "district": "Itaquera",
        "city": "São Paulo",
        "cep": "08270-120",
        "code": "27FCT2"
    },
    {
        "state": "SP",
        "name": "Posto de Saúde - Postão",
        "phone": "(17) 3281-4122 / 3281-4680",
        "email": "nucleomedico@uol.com.br",
        "place": "Posto de Saúde-POSTÃO",
        "district": "Centro",
        "city": "Olímpia",
        "cep": "15400-000",
        "code": "38FWD6"
    },
    {
        "state": "SP",
        "name": "Centro de Saúde de Registro",
        "phone": "(13) 3821-2454",
        "email": "alceuhidetabuti@gmail.com",
        "place": "Centro de Saúde de Registro",
        "district": "Centro",
        "city": "Registro",
        "cep": "11800-000",
        "code": "7A5248"
    },
    {
        "state": "SP",
        "name": "Centro de Saúde Dr. Takashi Enokibara",
        "phone": "(18) 3821-4630",
        "email": "marilda@morgadoeabreu.com.br",
        "place": "Centro de Saúde Dr. Takashi Enokibara",
        "district": "Centro",
        "city": "Dracena",
        "cep": "17900-000",
        "code": "E04255"
    },
    {
        "state": "SP",
        "name": "Ambulatório Médico de Especialidades do Estado de São Paulo - AME",
        "phone": "(19) 3634-1120",
        "email": "jsouza@amesaojoao.unicamp.br",
        "place": "Ambulatório Médico de Especialidades do Estado de São Paulo - AME",
        "district": "Centro",
        "city": "São João da Boa Vista",
        "cep": "13870-079",
        "code": "B52B27"
    },
    {
        "state": "SP",
        "name": "Clinica de dermatologia  da Santa Casa de São Paulo",
        "phone": "(11) 2176-7240/ 3223-0501",
        "email": "adri.spm@terra.com.br",
        "place": "Hospital da Santa Casa de São Paulo - Prédio Conde de Lara, andar Térreo",
        "district": "Vila Buarque",
        "city": "São Paulo",
        "cep": "01221-020",
        "code": "5FD787"
    },
    {
        "state": "SP",
        "name": "IV DISTRITO DERMATOLOGICO DE  SOROCABA",
        "phone": "(15) 3231-1465",
        "email": "gevaleria_16@hotmail.com",
        "place": "Policlínica Municipal de Sorocaba",
        "district": "St. Rosália",
        "city": "Sorocaba",
        "cep": "18090-210",
        "code": "5CF726"
    },
    {
        "state": "SP",
        "name": "Hospital Dia Rede Hora Certa - UNISA",
        "phone": "(11) 2141-8500",
        "email": "aninhaer@gmail.com",
        "place": "Hospital Dia Rede Hora Certa Capela do Socorro - UNISA-Ambulatório de Dermatologia - Piso Térreo sala 29, 39",
        "district": "Jardim das Imbuais",
        "city": "São Paulo",
        "cep": "04829-310",
        "code": "B8D424"
    },
    {
        "state": "SP",
        "name": "Serv. De Dermatologia da Santa Casa de Misericódia de São José dos Campos",
        "phone": "(12)3876-1999",
        "email": "samuel_mandelbaum@hotmail.com",
        "place": "Clinica Centro da Santa Casa de São José dos Campos",
        "district": "Centro",
        "city": "São José dos Campos",
        "cep": "12210-000",
        "code": "8F044B"
    },
    {
        "state": "RO",
        "name": "Hospital de Ji-Paraná",
        "phone": "(69)3421-3407",
        "email": "silmar.camarini@terra.com.br",
        "place": "Instituto Estadual de Educação Marechal Rondon",
        "district": "Casa preto",
        "city": "Ji-Paraná",
        "cep": "78961-410",
        "code": "CB2524"
    },
    {
        "state": "RR",
        "name": "Hospital Coronel Mota",
        "phone": "(95)3224-9285",
        "email": "apvitti@gmail.com",
        "place": "Hospital Coronel Mota",
        "district": "Centro",
        "city": "Boa Vista",
        "cep": "69301-150",
        "code": "15669A"
    }
];

/*SALVAR DADOS*/
router.get('/data', async (req, res) => {
    for (let i = 0; i < postos.length; i++) {
        postos[i].cep = parseInt(postos[i].cep.replace(/[^\d]+/g, ''));
        postos[i].phone = parseInt(postos[i].phone.replace(/[^\d]+/g, ''));
        await Service.create(postos[i]).then(success => {
            res.json(success);
        }).catch(error => {
            res.json(error);
        });
    }
});

/*LISTAR REGISTROS*/
router.get('/', auth, async (req, res) => {
    const perPage = parseInt(req.query['size']), page = Math.max(0, parseInt(req.query['page']));
    let search = req.query;

    delete search.size;
    delete search.page;
    if (search.name === '') {
        delete search.name;
    }
    if (search.city === '') {
        delete search.city;
    }
    if (search.state === '') {
        delete search.state;
    }
    if (search.name) {
        search.name = {'$regex': search.name.toLowerCase(), '$options': 'i'};
    }


    let count = await Service.find();
    count = count.length;



    const doctorService = require("../services/doctor");
    // let doctors = await doctorService.find({}).select(['hospital_id']).limit(99999)
    
    const quizService = require("../services/quiz"); 
    // let quiz = await quizService.find({}).select(['hospital_id']).limit(99999)
    // console.log(quiz)
    Service.find(search).select(['name', 'code', 'state', 'city', 'email', 'phone', 'hospital_id'])
        .limit(perPage)
        .skip(perPage * page)
        .sort('name')
        .then(async (doc) => {
            let selects = await Service.find().select(['state', 'city']);

            for(let i in doc){
                // doc[i].doctors = await doctorService.count({hospital_id: [doc[i]._id]});
                doc[i].doctors = await doctorService.count({hospital_id: doc[i]._id});
                doc[i].diagnostics = await quizService.count({hospital_id: [doc[i]._id]});
            }


            res.json({
                selects: selects,
                data: doc,
                page: page,
                limit: count,
                total: Math.ceil(count / perPage)
            });
        });
});

/*PEGAR REGISTRO POR ID*/
router.get('/list', auth, async (req, res) => {
    Service.findById(req.params.id).then(async (doc) => {
        res.json(doc);
    });
});

/*PEGAR REGISTRO POR ID*/
router.get('/:id', auth, async (req, res) => {
    Service.findById(req.params.id).then(async (doc) => {
        res.json(doc);
    });
});

/*SALVAR DADOS*/
router.post('/', auth, async (req, res) => {
    const data = req.body;
    let query = new Service(data);
    await query.save().then(success => {
        message.show(req, res, success);
    }).catch(error => {
        message.error(req, res, error);
    });
});

/*ATUALIZAR DADOS*/
router.put('/:id', auth, async (req, res) => {
    let id = req.params.id;
    const data = req.body;
    data.updated_at = new Date();

    await Service.update({_id: id}, data).then(async (success) => {
        message.show(req, res, success);
    }).catch(error => {
        message.error(req, res, error);
    });
});

/*DELETAR DADOS*/
router.delete('/:id', auth, async (req, res) => {
    let id = req.params.id;
    let haveDoctors = await doctorService.getByDoctorForHospital(id);

    let quizSvc = require('../services/quiz')
    let doctorSvc = require('../services/doctor')
    if (haveDoctors.length === 0) {
        Service.findById(id).then(async (success) => {
            Service.findOneAndDelete({_id: id}).then((success) => {
                message.show(req, res, success);
            });

            quizSvc.deleteMany({hospital_id: [id]}).then(data => {
                console.log('DEU CERTO', data)
            }).catch(err => {
                console.log('DEU ERRO', err)
            })

            doctorSvc.deleteMany({hospital_id: id}).then(data => {
                console.log('DEU CERTO DOCTOR', data)
            }).catch(err => {
                console.log('DEU ERRO DOCTOR', err)
            })
        }).catch((error) => {
            message.error(req, res, {});
        });
    } else {
        req.method = 'HAVE_DOCTOR';
        res.status(400);
        message.error(req, res, {});
    }
});

/*PEGAR HOSPITAL POR CODIGO*/
router.get('/code/:code', async (req, res) => {
    Service.find({code: req.params.code.toUpperCase()}).then(async (success) => {
        if (success.length !== 0) {
            message.show(req, res, success[0]);
        } else {
            req.method = 'NO_CODE';
            res.status(400);
            message.error(req, res, {});
        }
    }).catch((error) => {
        req.method = 'NO_CODE';
        res.status(400);
        message.error(req, res, {});
    });
});

/*PEGAR REGISTRO POR ID*/
router.post('/resident', auth_doctor, async (req, res) => {
    const data = req.body;
    Service.find({_id: {$in: data}}).then(async (doc) => {
        res.json(doc);
    });
});

/*ENVIAR EMAIL PARA RESPONSAVEL DO HOSPITAL E PARA O HOSPITAL*/
router.get('/send/:id', auth, async (req, res) => {

    Service.findById(req.params.id).then(async (doc) => {
        if (doc) {
            if (doc.email) {
                mailer.send([{
                    email: doc.email,
                    name: doc.name
                }], 'Aplicativo Planilha Câncer de Pele', await templates.sendCode(doc)).then((success) => {
                    req.method = 'MAIL';
                    message.show(req, res, success);
                }).catch((error) => {
                    req.method = 'MAIL';
                    message.error(req, res, error);
                });
            } else {
                req.method = 'NO_MAIL';
                res.status(200);
                message.error(req, res, {});
            }
        }
    });
});

/*EXPORTA ROUTER USER*/
module.exports = router;
