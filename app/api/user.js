/**
 * Descrição: USUARIO
 * Author: Thiago Silva
 */

/*IMPORTANDO BIBLIOTECAS*/
const express = require("express");
const router = express.Router();
const Service = require("../services/user");
const message = require("../core/message");
const auth = require("../core/auth");
const utils = require("../core/utils");

/*LISTAR REGISTROS*/
router.get('/', auth, async (req, res) => {
    const perPage = parseInt(req.query['size']), page = Math.max(0, parseInt(req.query['page']));
    let search = req.query;
    delete search.size;
    delete search.page;
    if (search.name === '') {
        delete search.name;
    }
    if (search.email === '') {
        delete search.email;
    }
    if (search.name) {
        search.name = {'$regex': search.name.toLowerCase(), '$options': 'i'};
    }
    if (search.email) {
        search.email = {'$regex': search.email.toLowerCase(), '$options': 'i'};
    }
    Service.find(search).select(['name', 'email'])
        .limit(perPage)
        .skip(perPage * page)
        .sort('-name')
        .then((doc) => {
            Service.find(search).countDocuments().exec((err, count) => {
                res.json({
                    data: doc,
                    page: page,
                    limit: count,
                    total: Math.ceil(count / perPage)
                });
            });
        });
});

/*PEGAR REGISTRO POR ID*/
router.get('/:id', auth, async (req, res) => {
    Service.findById(req.params.id).then((doc) => {
        res.json(doc);
    });
});

/*SALVAR DADOS*/
router.post('/', auth, async (req, res) => {
    const data = req.body;
    let query = new Service(data);

    await query.save().then(success => {
        message.show(req, res, success);
    }).catch(error => {
        message.error(req, res, error);
    });
});

/*ATUALIZAR DADOS*/
router.put('/:id', auth, async (req, res) => {
    let id = req.params.id;
    const data = req.body;
    data.updated_at = new Date();

    if (data.password !== null && data.password !== undefined) {
        data.password = await utils.cryptPassword(data.password);
    }

    await Service.update({_id: id}, data).then(async (success) => {
        message.show(req, res, success);
    }).catch(error => {
        message.error(req, res, error);
    });
});

/*DELETAR DADOS*/
router.delete('/:id', auth, async (req, res) => {
    let id = req.params.id;
    Service.findById(id).then((success) => {
        if (!success.master) {
            Service.findOneAndDelete({_id: id}).then((success) => {
                message.show(req, res, success);
            })
        } else {
            req.method = 'DELETE_USER';
            res.status(400);
            message.error(req, res, {});
        }
    }).catch((error) => {
        message.error(req, res, {});
    })
});

/*EXPORTA ROUTER USER*/
module.exports = router;
