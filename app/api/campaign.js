/**
 * Descrição: QUESTIONARIO
 * Author: Thiago Silva
 */

/*IMPORTANDO BIBLIOTECAS*/
const express = require("express");
const router = express.Router();
const Service = require("../services/campaign");
const message = require("../core/message");
const auth = require("../core/auth");

const data = {
    type: "form",
    question: [
        {
            label: "Identificação (Iniciais)",
            name: "identificacao",
            required: true,
            type: 'text',
            note: ''
        },
        {
            label: "Nome Completo",
            name: "nome",
            required: false,
            type: 'text',
            note: 'Preencher esse campo apenas se o paciente possuir histórico de CA pele'
        },
        {
            label: "Idade",
            name: "idade",
            required: true,
            type: 'number',
            note: ''
        },
        {
            label: "Sexo",
            name: "sexo",
            type: "radio",
            note: '',
            data: [
                {value: 1, label: "Masculino"},
                {value: 2, label: "Feminino"}
            ]
        },
        {
            label: "Fototipo",
            name: "foto_tipo",
            type: "radio",
            note: '',
            data: [
                {value: 1, label: "I"},
                {value: 2, label: "II"},
                {value: 3, label: "III"},
                {value: 4, label: "IV"},
                {value: 5, label: "V"},
                {value: 6, label: "VI"}
            ]
        },
        {
            label: "Exposição ao sol",
            name: "exposicao_sol",
            type: "radio",
            note: '',
            data: [
                {value: 1, label: "Com proteção"},
                {value: 2, label: "Sem proteção"},
                {value: 3, label: "Não se expõe"}
            ]
        },
        {
            label: "Histórico CA pele",
            name: "historico_ca_pele",
            type: "radio",
            note: '',
            data: [
                {value: 1, label: "Sim"},
                {value: 2, label: "Não"}
            ]
        },
        {
            label: "Histórico família CA pele",
            name: "historico_familia_ca_pele",
            type: "radio",
            note: '',
            data: [
                {value: 1, label: "Sim"},
                {value: 2, label: "Não"}
            ]
        },
        {
            label: "Pessoa de Risco CA pele",
            name: "pessoa_risco_ca_pele",
            type: "radio",
            note: '',
            data: [
                {value: 1, label: "Sim"},
                {value: 2, label: "Não"}
            ]
        },
        {
            label: "Como soube da campanha",
            name: "como_soube_campanha",
            type: "checkbox",
            note: '',
            data: [
                {value: 1, label: "Tv"},
                {value: 2, label: "Rádio"},
                {value: 3, label: "Cartaz/Panfleto"},
                {value: 4, label: "Palestras"},
                {value: 5, label: "Amigos/Família"},
                {value: 6, label: "Outros"}
            ]
        },
        {
            label: "Diagnóstico Clínico",
            name: "diagnostico_clinico",
            type: "checkbox",
            note: '',
            data: [
                {value: 1, label: "CBC"},
                {value: 2, label: "CEC"},
                {value: 3, label: "MM"},
                {value: 4, label: "Outro tumores malignos"},
                {value: 5, label: "Outros pré-neoplasias"},
                {value: 6, label: "Outras dermatoses"},
                {value: 7, label: "Ausência de dermatoses"}
            ]
        },
        {
            label: "Evolução",
            name: "evelocao",
            type: "radio",
            note: '',
            data: [
                {value: 1, label: "<= 1 Anos"},
                {value: 2, label: "1 a 2 Anos"},
                {value: 3, label: "2 a 3 Anos"},
                {value: 4, label: ">= 4 Anos"}
            ]
        },
        {
            label: "Localização",
            name: "localizacao",
            type: "checkbox",
            note: '',
            data: [
                {value: 1, label: "Cabeça"},
                {value: 2, label: "Tronco"},
                {value: 3, label: "Membros"}
            ]
        },
        {
            label: "Conduta",
            name: "conduta",
            type: "checkbox",
            note: '',
            data: [
                {value: 1, label: "Agendamento serviço"},
                {value: 2, label: "Biópsia"},
                {value: 3, label: "Encaminhamento para outro serviço"},
                {value: 4, label: "Cirurgia"},
                {value: 5, label: "Orientação"},
            ]
        }
    ]
};

/*LISTAR REGISTROS*/
router.get('/list', auth, async (req, res) => {
    Service.find().select(['title','quiz', 'logo', 'logo_color']).then(async (success) => {
        message.show(req, res, success);
    }).catch(error => {
        message.error(req, res, error);
    });
});


/*LISTAR REGISTROS*/
router.get('/:id', async (req, res) => {
    let id = req.params.id || null;
    if (id === null) {
        req.method = 'CAMPAING_NOTFOUND';
        res.status(400);
        message.error(req, res, {});
    } else {
        Service.find({_id: id}).then(async (success) => {

            message.show(req, res, success[0]);
        }).catch((error) => {
            req.method = 'CAMPAING';
            message.error(req, res, {});
        });
    }
});

/*ATUALIZAR DADOS*/
router.get('/update/:id', async (req, res) => {
    let id = req.params.id;
    await Service.updateOne({_id: id}, {quiz: data}).then(async (success) => {
        message.show(req, res, data);
    }).catch(error => {
        message.error(req, res, data);
    });
});

/*EXPORTA ROUTER USER*/
module.exports = router;
