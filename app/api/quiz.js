/**
 * Descrição: QUESTIONARIO
 * Author: Thiago Silva
 */

/*IMPORTANDO BIBLIOTECAS*/

const express = require("express");
const router = express.Router();
const Service = require("../services/quiz");
const campaignService = require("../services/campaign");
const doctorService = require("../services/doctor");
const serviceService = require("../services/hospital");
const message = require("../core/message");
const auth = require("../core/auth");
const auth_doctor = require("../core/auth.doctor");

/*LISTAR REGISTROS*/
router.get('/filters', async (req, res) => {
    res.json({
        desks: await serviceService.find().select(['name', 'state', 'city']),
        doctors: await doctorService.find().select(['name', 'hospital_id'])
    });
});

router.get('/all', auth, async (req, res) => {
    let perPage = parseInt(req.query['size']),
        page = Math.max(0, parseInt(req.query['page']));
    let search = req.query;

    delete search.page;
    delete search.size;


    let hospitalSvc = require('../services/hospital')
    let doctorSvc = require('../services/doctor')

    let doctor = await doctorSvc.find({}).limit(999999)
    let hospital = await hospitalSvc.find({}).limit(999999)
    let all_hospital = hospital;

    let data;
    if (search.filter) {
        delete search.filter;
        data = await Service.find(search).limit(999999)
    } else {
        data = await Service.find({}).limit(999999)
    }

    if (!search.filter) {
        hospital = hospital.filter(h => {

            let state = true;
            let city = true;

            if (search.city !== undefined) {
                city = h.city == search.city
            }
            if (search.state !== undefined) {
                state = h.state == search.state
            }

            return state && city

        }).map(h => String(h._id))
        if (search.hospital_id !== undefined)
            hospital = hospital.filter(h => h == String(search.hospital_id))

        data = data.filter(q => {
            let campaign_id = String(q.campaign_id) == String(search.campaign_id) || q.campaign_id === undefined

            let hospital_id = true;
            let doctor_id = true
            if (hospital.length > 0) {
                hospital_id = hospital.indexOf(String(q.hospital_id)) !== -1
            }

            if (search.doctor_id !== undefined) {
                doctor_id = String(q.doctor_id) == String(search.doctor_id)
            }

            return campaign_id && hospital_id && doctor_id
        })
    }

    res.json({
        data: {
            quiz: data,
            doctor: doctor.map(d => ({
                id: d._id,
                name: d.name
            })),
            hospital: all_hospital.map(d => ({
                id: d._id,
                name: d.name
            }))
        },
        page: page,
        limit: page,
        total: data.length
    });

    // Service.aggregate([{
    //         $lookup: {
    //             from: 'doctor',
    //             localField: 'doctor_id',
    //             foreignField: '_id',
    //             as: 'doctor'
    //         }
    //     },
    //     {
    //         $lookup: {
    //             from: 'hospital',
    //             localField: 'hospital_id',
    //             foreignField: '_id',
    //             as: 'service'
    //         }
    //     },
    //     {
    //         $unwind: "$doctor"
    //     },
    //     {
    //         $unwind: "$service"
    //     },
    //     {
    //         $limit: perPage
    //     },
    //     {
    //         $skip: (perPage * page)
    //     }
    // ]).then(async (doc) => {
    //     Service.find().countDocuments().then((count, error) => {
    //         res.json({
    //             data: doc,
    //             page: page,
    //             limit: count,
    //             total: Math.ceil(count / perPage)
    //         });
    //     });
    // });
});

router.get('/reporting', auth, async (req, res) => {
    let search = req.query;
    let campanha = await campaignService.findById(search.campaign_id);
    let ficha = campanha.quiz.question.filter(f => ['identificacao', 'nome', 'idade'].indexOf(f.name) === -1);
    // let data = await Service.find(search);


    let hospitalSvc = require('../services/hospital')


    let hospital = await hospitalSvc.find({}).limit(999999)
    let data = await Service.find({}).limit(999999)



    hospital = hospital.filter(h => {

        let state = true;
        let city = true;

        if (search.city !== undefined) {
            city = h.city == search.city
        }
        if (search.state !== undefined) {
            state = h.state == search.state
        }

        return state && city

    }).map(h => String(h._id))
    if (search.hospital_id !== undefined)
        hospital = hospital.filter(h => h == String(search.hospital_id))

    data = data.filter(q => {
        let campaign_id = String(q.campaign_id) == String(search.campaign_id) || q.campaign_id === undefined
        let hospital_id = true;
        if (hospital.length > 0) {
            hospital_id = hospital.indexOf(String(q.hospital_id)) !== -1
        }

        return campaign_id && hospital_id
    })

    let total_hospital = hospital.length;
    let total_quiz = data.length


    let orderBy = require('lodash/orderBy')


    ficha = ficha.map(f => {
        // let total = data.filter(t => t[f.name]).length;
        let diagnosticos = data.map(d => d[f.name])

        let total = diagnosticos.length;
        if (f.type === 'checkbox' && diagnosticos.length > 0) {
            total = diagnosticos.map(d => d.length).reduce((a, b) => a + b)
        }

        f.data = f.data.map(d => {


            if (f.type === 'radio') {
                d.total = diagnosticos.filter(d2 => d2 == d.value).length
            } else if (f.type === 'checkbox') {
                d.total = diagnosticos.filter(d2 => d2.indexOf(d.value) !== -1).length
            }
            d.pct = parseFloat((d.total * 100 / total))

            // d.total = data.filter(i => i[f.name] === d.value).length;
            // d.pct = Math.floor(d.total / total * 100) || 0;
            return d;
        });

        f.data = orderBy(f.data, ['total'], ['desc'])

        f.chart = {
            labels: f.data.map(c => c.label),
            datasets: [{
                label: f.label,
                data: f.data.map(p => p.total),
                backgroundColor: f.data.map(c => {
                    return random_rgba();
                })
            }]
        };

        f.abstract = {
            total: f.data.map(d => d.total).reduce((a, b) => a + b),
            pct: 100
        }

        return f;
    });

    res.json({
        ficha,
        total_hospital,
        total_quiz
    });
});

function random_rgba() {
    var o = Math.round,
        r = Math.random,
        s = 255;
    return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ',' + r().toFixed(1) + ')';
}

/*LISTAR REGISTROS*/
router.get('/all/:id', auth, async (req, res) => {
    Service.find({
        doctor_id: req.params.id
    }).then(async (doc) => {
        res.json(doc);
    });
});

/*LISTAR REGISTROS*/
router.post('/report', auth, async (req, res) => {
    const perPage = parseInt(req.query['size']),
        page = Math.max(0, parseInt(req.query['page']));
    let search = req.query;
    delete search.size;
    delete search.page;
    delete search.city;
    delete search.state;

    if (search.campaign_id === '') {
        delete search.campaign_id;
    }
    if (search.hospital_id === '') {
        delete search.hospital_id;
    }
    if (search.doctor_id === '') {
        delete search.doctor_id;
    }

    Service.find(search).select(req.body).then(async (doc) => {
        res.json(doc);
    });
});

/*PEGAR REGISTRO POR ID*/
router.get('/:id', auth_doctor, async (req, res) => {
    Service.find({
        doctor_id: req.params.id
    }).then(async (doc) => {
        res.json(doc);
    });
});

/*SALVAR DADOS*/
router.post('/', auth_doctor, async (req, res) => {
    const data = req.body;
    data.created_at = new Date();
    let query = new Service(data);
    await query.save().then(success => {
        message.show(req, res, success);
    }).catch(error => {
        message.error(req, res, error);
    });
});

/*ATUALIZAR DADOS*/
router.put('/:id', auth_doctor, async (req, res) => {
    let id = req.params.id;
    const data = req.body;
    data.updated_at = new Date();
    await Service.update({
        _id: id
    }, data).then(async (success) => {
        message.show(req, res, success);
    }).catch(error => {
        message.error(req, res, error);
    });
});

/*DELETAR DADOS*/
router.delete('/admin/:id', auth, async (req, res) => {
    console.log(req.params)
    let id = req.params.id;
    Service.findOneAndDelete({
        _id: id
    }).then((success) => {
        message.show(req, res, success);
    }).catch(error => {
        message.error(req, res, error);
    });
});

/*DELETAR DADOS*/
router.delete('/:id', auth_doctor, async (req, res) => {
    let id = req.params.id;
    Service.findOneAndDelete({
        _id: id
    }).then((success) => {
        message.show(req, res, success);
    }).catch(error => {
        message.error(req, res, error);
    });
});

let actions = {};
actions.delete = async (id) => {
    await Service.findOneAndDelete({
        _id: id
    });
};

actions.save = async (data) => {
    let query = await Service.find({
        uuid: data.uuid
    });

    if (query.length === 0) {
        await Service.create(data);
    } else {
        data.synced_at = new Date();
        data.update_at = new Date();
        await Service.updateMany({
            _id: data._id
        }, data);
    }
};

actions.create = async (data) => {
    Service.create(data);
};

/*DELETAR DADOS*/
router.post('/sync/:id', auth_doctor, async (req, res) => {
    const data = req.body;

    try {
        if (data.length !== 0) {
            for (let i = 0; i < data.length; i++) {
                let item = data[i];
                if (item.deleted_at !== null) {
                    await actions.delete(item._id);
                } else {
                    await actions.save(item);
                }
            }
        }

        let quiz = await Service.find({
            doctor_id: req.params.id
        });
        res.json(quiz);

    } catch (e) {}
});


/*DELETAR DADOS*/
router.post('/report-doctor', auth_doctor, async (req, res) => {


    let quizSvc = require('../services/quiz')

    let doctorSvc = require('../services/doctor')
    let hospitalSvc = require('../services/hospital')
    let doctor = await doctorSvc.find({}).limit(99999)
    let hospital = await hospitalSvc.find({}).limit(99999)
    let quiz = await quizSvc.find({}).limit(99999)
    quiz = quiz.map(h => ({
        doctor_id: h.doctor_id,
        hospital_id: h.hospital_id
    }))
    hospital = hospital.map(h => ({
        id: h.id,
        name: h.name
    }))
    doctor = doctor.map(d => {
        h = hospital.find(h => h.id == d.hospital_id)
        q = quiz.filter(q => String(q.doctor_id) == String(d._id)).length
        d = {
            id: d._id,
            hospital: h !== undefined ? h.name : null,
            name: d.name,
            email: d.email,
            total: q
        }

        return d
    }).filter(d => d.hospital !== null)

    res.json({
        doctor
    })

});
/*DELETAR DADOS*/
router.post('/report-hospital', auth_doctor, async (req, res) => {


    let doctorSvc = require('../services/doctor')
    let hospitalSvc = require('../services/hospital')
    let quizSvc = require('../services/quiz')
    let doctor = await doctorSvc.find({}).limit(99999)
    let hospital = await hospitalSvc.find({}).limit(99999)
    let quiz = await quizSvc.find({}).limit(99999)
    quiz = quiz.map(h => ({
        doctor_id: h.doctor_id,
        hospital_id: h.hospital_id
    }))
    hospital = hospital.map(h => ({
        id: h.id,
        name: h.name,
        total: quiz.filter(q => String(q.hospital_id) == String(h._id)).length
    }))
    // doctor = doctor.map(d => {
    //     h = hospital.find(h => h.id == d.hospital_id)
    //     q = quiz.filter(q => String(q.doctor_id) == String(d._id)).length
    //     d = {
    //         id: d._id,
    //         hospital: h !== undefined ? h.name : null, 
    //         name: d.name, 
    //         email: d.email,
    //         total: q
    //     }

    //     return d
    // }).filter(d => d.hospital !== null)

    res.json({
        hospital
    })

});


/*DELETAR DADOS*/
router.post('/import', auth, async (req, res) => {

    const doctorSvc = require('../services/doctor')
    const hospitalSvc = require('../services/hospital')
    const quizSvc = require('../services/quiz')


    if (req.body.search !== undefined) {

        let doctor = await doctorSvc.findOne({
            '_id': req.body.search
        })

        let hospital = await hospitalSvc.find({
            '_id': {
                $in: doctor.hospital_id
            }
        })

        let diagnostics = await quizSvc.find({
            'doctor_id': doctor._id
        })
        res.json({
            doctor,
            hospital,
            diagnostics
        })

    } else {
        await req.body.diagnostics.forEach(async (d) => {
            d.synced_at = new Date();
            await actions.save(d)
        })
        res.json({
            status: true
        })
    }

});


/*DELETAR DADOS*/
router.post('/export', auth, async (req, res) => {

    const doctorSvc = require('../services/doctor')
    const hospitalSvc = require('../services/hospital')
    const quizSvc = require('../services/quiz')


    quiz = await quizSvc.aggregate(
        [{
                "$lookup": {
                    "from": "hospital",
                    "localField": "hospital_id",
                    "foreignField": "_id",
                    "as": "hospital"
                }
            },
            {
                "$lookup": {
                    "from": "doctor",
                    "localField": "doctor_id",
                    "foreignField": "_id",
                    "as": "doctor"
                }
            },
            {
                "$unwind": "$hospital"
            },
            {
                "$unwind": "$doctor"
            }
        ]
    )

    res.json(quiz.map(q => {
        let obj = {}
        let doctor = q.doctor;
        let hospital = q.hospital;

        Object.keys(q).forEach(key => {
            if (['status', 'updated_at', 'campaign_id', 'hospital_id', 'doctor_id', 'uuid', 'deleted_at', 'doctor', 'hospital', '__v', 'update_at'].indexOf(key) === -1) {
                if (Array.isArray(q[key]))
                    q[key] = q[key].join(',')
                // else if(q[key] instanceof Date)
                //     q[key] = q[key].toLocaleDateString('pt-BR') + ' ' + q[key].toLocaleTimeString('pt-BR')

                obj[`quiz_${key}`] = q[key]
            }

        })
        Object.keys(doctor).forEach(key => {
            if (['crm', 'phone', 'email', 'name', '_id'].indexOf(key) !== -1)
                obj[`doctor_${key}`] = String(doctor[key])
        })

        Object.keys(hospital).forEach(key => {
            if (['attach', 'logo', '__v', 'updated_at', 'created_at'].indexOf(key) === -1)
                obj[`hospital_${key}`] = String(hospital[key])
        })
        delete q.doctor;
        delete q.hospital;
        return obj;
    }))

});

/*DELETAR DADOS*/
router.post('/report-error-sync', auth_doctor, async (req, res) => {

    const data = req.body;

    const mailer = require("../core/email");

    let html = `O seguinte usuário reportou erro de sincronização:
    <br><b>Nome: </b>${data.name}
    <br><b>E-mail: </b>${data.email}
    <br><b>Código: </b>${data.id}
    
    <br><br>Segue abaixo o JSON com os diagnósticos, caso prefira baixar o JSON segue também anexado.

    <br><br>${JSON.stringify(data.diagnostics)}
    `

    mailer.send(
        [{
                email: 'flaviomirandadesouza@gmail.com',
                name: 'Flávio SOuza'
            },
            {
                email: 'andrade.itarget@gmail.com',
                name: 'Emanuel Andrade'
            }
        ],
        `REPORTE DE ERRO DE SINCRONIZAÇÃO [${data.name}]`,
        html, {
            "type": 'application/json',
            "name": 'diagnostics.json',
            "content": new Buffer(JSON.stringify(data.diagnostics)).toString('base64')
        }
    ).then((success) => {

        let msg = data.diagnostics.length === 1 ? 'seu diagnóstico.' : `seus ${data.diagnostics.length} diagnósticos.`
        res.json({
            title: data.name,
            'message': `Recebemos com sucesso os ${msg}`
        })
    }).catch((error) => {
        res.json({
            title: data.name,
            'message': `Infelizmente não conseguimos sincronizar seu(s) diagnóstico(s), contate o administrador e tente novamente mais tarde.`
        })
    });


});

/*EXPORTA ROUTER USER*/
module.exports = router;