/**
 * Descrição: USUARIO
 * Author: Thiago Silva
 */

/*IMPORTANDO BIBLIOTECAS*/
const express = require("express");
const router = express.Router();
const Service = require("../services/login");
const message = require("../core/message");
const mailer = require("../core/email");
const templates = require("../core/templates");
const settings = require("../core/settings");

/*LOGIN*/
router.post('/', async (req, res) => {
    let {email, password} = req.body;
    const result = await Service.findByCredentials(email, password);
    if (result) {
        const token = await result.generateAuthToken();
        res.status(200).json({
            status: 200,
            message: `Olá, ${result.name} seja bem vindo ao sistema de gerenciamento do APP SBD`,
            data: {name: result.name, email: result.email, id: result._id},
            token: token
        });
    } else {
        res.status(400).json({
            status: 400,
            message: `Dados inválidos, tente novamente!`
        });
    }
});

/*RECOVER */
router.post('/recover', async (req, res) => {
    const {email} = req.body;
    const result = await Service.findByEmail(email);
    if (result) {
        const password = await result.generatePassword();
        let TEMPLATE = await templates.recover({
            name: result.name,
            password: password
        });
        mailer.send([{email: result.email, name: result.name}], 'RECUPERAÇÃO DE SENHA', TEMPLATE, {logo: settings.LOGO}).then((success) => {
            message.show(req, res, success);
        }).catch((error) => {
            message.error(req, res, error);
        });
    } else {
        message.error(req, res, {});
    }
});

router.get('/online', async (req, res) => {
    res.json({status:200, message: 'connected'});
});

/*EXPORTA ROUTER USER*/
module.exports = router;
