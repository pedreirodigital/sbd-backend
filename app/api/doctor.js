/**
 * Descrição: MÉDICO
 * Author: Thiago Silva
 */

/*IMPORTANDO BIBLIOTECAS*/
const express = require("express");
const router = express.Router();
const Service = require("../services/doctor");
const ServiceHospital = require("../services/hospital");
const ServiceQuiz = require("../services/quiz");
const message = require("../core/message");
const auth = require("../core/auth");
const auth_doctor = require("../core/auth.doctor");
const mailer = require("../core/email");
const templates = require("../core/templates");
const settings = require("../core/settings");
const utils = require("../core/utils");
const jwt = require('jsonwebtoken');
const moment = require('moment');

/*LISTAR REGISTROS*/
router.get('/:id', auth, async (req, res) => {
    let id = req.params.id;
    const perPage = parseInt(req.query['size']), page = Math.max(0, parseInt(req.query['page']));
    let quizSvc = require('../services/quiz')
    Service.find({hospital_id: id}).select(['name', 'crm', 'email', 'phone', 'status'])
        .limit(perPage)
        .skip(perPage * page)
        .sort('name')
        .then(async (doc) => {

            let cd = require('lodash/cloneDeep')
            let teste = []
            for(let i in doc){
                let t = cd(doc[i]._doc)
                let diagnostics = await quizSvc.count({doctor_id: String(t._id)});
                teste.push(
                    {
                        diagnostics,
                        ...t
                    }
                    // 'name', 'crm', 'email', 'phone', 'status'
                )
            }

            // let teste = await doc.map(async d => {
            //     let d2 = cd(d._doc)
            //     console.log({doctor_id: String(d2._id)})
            //     return d2
            // })

            // doc = doc.map(d => {
            //     d.flavio = 123;
            //     return d
            // })
            console.log(teste)

            // 
            Service.countDocuments().exec((err, count) => {
                res.json({
                    data: teste,
                    page: page,
                    limit: count,
                    total: Math.ceil(count / perPage)
                });
            });
        });
});

/*PEGAR REGISTRO POR ID*/
router.get('/get/:id', async (req, res) => {
    Service.findById(req.params.id).select(['name', 'email']).then((doc) => {
        res.json(doc);
    });
});

/*SALVAR DADOS*/
router.post('/', async (req, res) => {
    const data = req.body;
    data.services = {};
    data.services[data.hospital_id] = true;
    let query = new Service(data);
    let exists = await Service.doctorExists(data);

    if (exists)
        exists = exists[0];

    if (!exists) {
        await query.save().then(success => {
            req.method = 'DOCTOR';
            message.show(req, res, success);
        }).catch(error => {
            req.method = 'DOCTOR';
            message.error(req, res, error);
        });
    } else {
        req.method = 'DOCTOR';
        res.status(400);
        message.error(req, res, {});
    }
})

/*ATUALIZAR DADOS*/
router.put('/:id', auth, async (req, res) => {
    let id = req.params.id;
    const data = req.body;

    await Service.update({_id: id}, data).then(async (success) => {
        message.show(req, res, success);
    }).catch(error => {
        message.error(req, res, error);
    });
});

/*DELETAR DADOS*/
router.delete('/:id', auth, async (req, res) => {
    let id = req.params.id;
    Service.findById(id).then((success) => {
        Service.findOneAndDelete({_id: id}).then((success) => {
            message.show(req, res, success);
        });
    }).catch((error) => {
        message.error(req, res, {});
    })
});

/*RECOVER */
router.post('/recover', async (req, res) => {
    const {email, campaing} = req.body;
    const result = await Service.findByEmailDoctor(email);
    if (result) {
        const password = await result.generatePasswordDoctor();
        let TEMPLATE = await templates.recover_doctor({
            name: result.name,
            password: password,
            campaing: campaing
        });
        mailer.send([{email: result.email, name: result.name}], 'RECUPERAÇÃO DE SENHA', TEMPLATE).then((success) => {
            req.method = 'RECOVER_DOCTOR';
            message.show(req, res, success);
        }).catch((error) => {
            req.method = 'DOCTOR_NOT_FOUND';
            message.error(req, res, {});
        });
    } else {
        req.method = 'DOCTOR_NOT_FOUND';
        message.error(req, res, {});
    }
});

/*LOGIN*/
router.post('/login', async (req, res) => {
    let {email, password} = req.body;
    const result = await Service.findByCredentialsDoctor(email, password);
    if (result) {
        const token = await result.generateAuthTokenDoctor();

        let service_current = null;
        Object.keys(result.services).map(x => {
            if (result.services[x]) {
                service_current = x;
            }
        });

        let newResult = {
            id: result.id,
            crm: result.crm,
            name: result.name,
            phone: result.phone,
            email: result.email,
            admin: result.admin,
            photo: result.photo,
            total: {service: Object.keys(result.services).length, diagnostics: await getDisgnotics(result.id)},
            services: await getService(result.services, service_current),
        };

        res.status(200).json({
            status: 200,
            message: `Olá, ${result.name} seja bem vindo ao sistema de gerenciamento do APP SBD`,
            data: newResult,
            token: token
        });
    } else {
        res.status(400).json({
            status: 400,
            message: `Dados inválidos, tente novamente!`
        });
    }
});

/*LOGIN*/
router.get('/data/:id', auth_doctor, async (req, res) => {
    let id = req.params.id;
    const result = await Service.findById(id);

    let tokens = [];
    await result.tokens.filter(async t => {
        let valide = await jwt.verify(t.token, settings.JWT_KEY_APP);
        let currentDate = moment(new Date());
        let tokenDate = moment(new Date(valide.logged));
        if (currentDate.diff(tokenDate, 'hours') < 24) {
            tokens.push(t);
        }
    });
    result.tokens = tokens;
    await Service.updateOne({_id: result.id}, {tokens: tokens});

    let newResult = {
        id: result.id,
        crm: result.crm,
        name: result.name,
        phone: result.phone,
        email: result.email,
        admin: result.admin,
        photo: result.photo,
        total: {service: Object.keys(result.services).length, diagnostics: await getDisgnotics(result.id)},
        services: await getService(result.services),
        token: tokens[tokens.length - 1].token
    };

    let quiz = await ServiceQuiz.find({doctor_id: result.id});
    res.status(200).json({
        status: 200,
        message: `carregando dados do usuário`,
        data: newResult,
        quiz: quiz
    });
});

/*PEGAR REGISTRO POR ID*/
router.put('/update/:id', auth_doctor, async (req, res) => {
    const data = req.body;
    let id = req.params.id;

    let services = {};
    let service_current = null;
    data.services.filter(x => {
        services[x.id] = x.current;
        if (x.current) {
            service_current = x.id;
        }
    });
    data.services = services;

    await Service.updateOne({_id: id}, {services: data.services}).then(async (success) => {
        let serviceUpdate = await getService(services, service_current);
        message.show(req, res, serviceUpdate);
    }).catch(error => {
        message.error(req, res, error);
    });
});

/*PEGAR REGISTRO POR ID*/
router.put('/profile/:id', auth_doctor, async (req, res) => {
    const data = req.body;
    let id = req.params.id;
    data.updated_at = new Date();

    await Service.update({_id: id}, data).then(async (success) => {
        let _data = await getService(data.services);
        req.method = 'DOCTOR_UPDATE_DATA';
        message.show(req, res, _data[0]);
    }).catch(error => {
        message.error(req, res, error);
    });
});

let getDisgnotics = async (id) => {
    if (id) {
        let data = await ServiceQuiz.find({doctor_id: id});
        return data.length;
    } else {
        return 0;
    }
};

let getService = async (ids, current = null) => {
    if (ids) {
        let data = await ServiceHospital.find({_id: {$in: Object.keys(ids)}}).then(doc => {
            let i = 0;
            return doc.map(s => {
                let m = {};
                m.id = s.id;
                m.name = s.name;
                m.logo = s.attach;
                m.place = `${s.place}, ${s.number} - ${s.district} - ${s.cep} - ${s.city}/${s.state} `;
                m.phone = s.phone;
                m.logo = s.attach;
                if (current === null) {
                    m.current = ids[m.id];
                } else {
                    m.current = s.id === current ? true : false;
                }
                m.created_at = s.created_at;
                i++;
                return m;
            });
        });
        return data;
    } else {
        return [];
    }
};

/*PEGAR REGISTRO POR ID*/
router.put('/service/:id', auth_doctor, async (req, res) => {
    const data = req.body;
    let newData = [];
    let id = req.params.id;

    data.code = data.code.toUpperCase();

    let doctor = await Service.findById(id);
    let hospital = await ServiceHospital.findOne(data);

    if (hospital) {
        newData = [hospital];
        newData = newData.map(s => {
            let m = {};
            m.id = s.id;
            m.name = s.name;
            m.logo = s.attach;
            m.place = `${s.place}, ${s.number} - ${s.district} - ${s.cep} - ${s.city}/${s.state} `;
            m.phone = s.phone;
            m.logo = s.attach;
            m.current = false;
            m.created_at = s.created_at;
            return m;
        });
    }

    if (hospital === null) {
        res.status(400);
        req.method = 'SERVICE_NOT_FOUND';
        message.error(req, res, {});
    } else {

        if (Object.keys(doctor.services).indexOf(hospital.id) !== -1) {
            req.method = 'SERVICE_EXISTS';
            res.status(400);
            message.error(req, res, {exists: true});
        } else {
            doctor.hospital_id = doctor.hospital_id.concat(hospital._id);
            doctor.services[hospital._id] = false;
            Service.updateOne({_id: id}, doctor).then(async (success) => {
                req.method = 'SERVICE_PUT';
                message.show(req, res, newData);
            }).catch((err) => {
                res.status(400);
                req.method = 'SERVICE_PUT';
                message.error(req, res, err);
            });
        }
    }
});
/*EXPORTA ROUTER USER*/
module.exports = router;
