/**
 * Descrição: USUARIO
 * Author: Thiago Silva
 */

/*IMPORTANDO BIBLIOTECAS*/
const express = require("express");
const router = express.Router();
const Service = require("../services/attachments");
const message = require("../core/message");
const settings = require("../core/settings");
const multer = require('multer');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});

var upload = multer({storage: storage});

/*UPLOAD*/
router.post('/', upload.single('file'), async function (req, res) {
    let query = new Service({
        name: req.file.filename,
        meta: req.file
    });
    await query.save().then(success => {
        req.method = 'UPLOAD';
        message.show(req, res, success);
    }).catch(error => {
        req.method = 'UPLOAD';
        message.error(req, res, error);
    });
});

/*GET*/
router.get('/:id', async function (req, res) {
    let id = req.params.id;
    Service.findById(id).then((success) => {
        let data = {};
        data.name = success.name;
        data.file = `uploads/${success.meta.filename}`;
        message.show(req, res, data);
    }).catch((error) => {
        req.method = 'LOAD_IMAGE';
        message.error(req, res, error);
    });
});

/*DELETAR DADOS*/
router.delete('/:id', async (req, res) => {
    let id = req.params.id;
    Service.findById(id).then((success) => {
        Service.findOneAndDelete({_id: id}).then((success) => {
            message.show(req, res, success);
        });
    }).catch((error) => {
        message.error(req, res, {});
    });
});

/*EXPORTA ROUTER USER*/
module.exports = router;
