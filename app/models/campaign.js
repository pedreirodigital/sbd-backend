/**
 * Descrição: MODELO QUESTIONARIO
 * Author: Thiago Silva
 */

/*IMPORTANDO BIBLIOTECAS*/
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const status = require('../enums/status');

/*CRIANDO MODELO*/
module.exports = new Schema({
    title: {type: String},
    logo: {type: String},
    theme: {type: Object},
    logo_invert: {type: Boolean},
    background: {type: String},
    quiz: {type: Object},

    status: {type: Number, required: true, default: status.ACTIVE},
    created_at: {type: Date, default: new Date()},
    updated_at: {type: Date, default: null},
}, {collection: 'campaign'});
