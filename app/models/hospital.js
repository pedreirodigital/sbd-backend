/**
 * Descrição: MODELO HOSPITAL
 * Author: Thiago Silva
 */

/*IMPORTANDO BIBLIOTECAS*/
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const status = require('../enums/status');

/*CRIANDO MODELO*/
module.exports = new Schema({
    logo: {type: String},
    attach: {type: String},

    name: {type: String, required: true, trim: true},
    cep: {type: Number, trim: true, default: null},
    place: {type: String, trim: true, default: null},
    number: {type: String, trim: true, default: null},
    complement: {type: String, trim: true, default: null},
    district: {type: String, trim: true, default: null},
    city: {type: String, trim: true, default: null},
    state: {type: String, trim: true, default: null},
    phone: {type: Number, trim: true, default: null},
    email: {type: String, trim: true, default: null},
    site: {type: String, trim: true, default: null},
    code: {type: String},

    accredited: {type: String, trim: true, default: null},
    public_service: {type: String, trim: true, default: null},
    service_forecast: {type: String, trim: true, default: null},
    volunteer_forecasting: {type: String, trim: true, default: null},
    cnpcp: {type: String, trim: true, default: null},
    attendance: {type: String, trim: true, default: null},
    resolutive: {type: String, trim: true, default: null},
    forwarding: {type: String, trim: true, default: null},
    lecture: {type: String, trim: true, default: null},
    surgery: {type: String, trim: true, default: null},
    comments: {type: String, trim: true, default: null},
    coordinators: {type: String, trim: true, default: null},

    responsible_name: {type: String, trim: true, default: null},
    responsible_phone: {type: String, trim: true, default: null},
    responsible_email: {type: String, trim: true, default: null},

    coordinator_name: {type: String, trim: true, default: null},
    coordinator_phone: {type: String, trim: true, default: null},
    coordinator_email: {type: String, trim: true, default: null},

    doctors: {type: Number},
    diagnostics: {type: Number},

    status: {type: Number, required: true, default: status.ACTIVE},
    created_at: {type: Date, default: new Date()},
    updated_at: {type: Date, default: null},
}, {collection: 'hospital'});
