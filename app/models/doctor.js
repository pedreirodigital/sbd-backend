/**
 * Descrição: MODELO MÉDICO
 * Author: Thiago Silva
 */

/*IMPORTANDO BIBLIOTECAS*/
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const status = require('../enums/status');

/*CRIANDO MODELO*/
module.exports = new Schema({
    hospital_id: [{type: Schema.ObjectId}],
    services: {type: Object},
    photo: {type: String},
    name: {type: String, trim: true},
    crm: {type: String, trim: true, default: null},
    phone: {type: String, trim: true, default: null},
    email: {type: String, required: true, trim: true, default: null},
    password: {type: String, trim: true},
    admin: {type: Boolean, default: false},
    recover: {type: String, default: null},

    status: {type: Number, default: status.ACTIVE},
    created_at: {type: Date, default: new Date()},
    updated_at: {type: Date, default: null},
    updated_recover_at: {type: Date, default: null},
    tokens: [{token: {type: String}}]
}, {collection: 'doctor'});
