/**
 * Descrição: MODELO USUARIO
 * Author: Thiago Silva
 */

/*IMPORTANDO BIBLIOTECAS*/
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/*CRIANDO MODELO*/
module.exports = new Schema({
    name: {type: String},
    meta: {type: Object},
    created_at: {type: Date, default: new Date()}
}, {collection: 'attachments'});
