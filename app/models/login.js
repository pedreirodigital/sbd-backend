/**
 * Descrição: MODELO USUARIO
 * Author: Thiago Silva
 */

/*IMPORTANDO BIBLIOTECAS*/
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/*CRIANDO MODELO*/
module.exports = new Schema({
    name: {type: String, trim: true},
    email: {type: String, required: true, trim: true, lowercase: true},
    password: {type: String, trim: true},
    recover: {type: String, trim: true},
    tokens: {type: Array}
}, {collection: 'user'});
