/**
 * Descrição: MODELO USUARIO
 * Author: Thiago Silva
 */

/*IMPORTANDO BIBLIOTECAS*/
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const status = require('../enums/status');

/*CRIANDO MODELO*/
module.exports = new Schema({
    foto: {type: String},
    name: {type: String, required: true, trim: true},
    email: {type: String, required: true, trim: true, lowercase: true},
    password: {type: String, trim: true},
    status: {type: Number, required: true, default: status.ACTIVE},
    master: {type: Boolean, default: false},
    created_at: {type: Date, default: new Date()},
    updated_at: {type: Date, default: null},
    updated_recover_at: {type: Date, default: null},
    recover: {type: String},

    tokens: [{token: {type: String}}]
}, {collection: 'user'});
