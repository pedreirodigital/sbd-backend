/**
 * Descrição: MODELO QUESTIONARIO
 * Author: Thiago Silva
 */

/*IMPORTANDO BIBLIOTECAS*/
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const status = require('../enums/status');

/*CRIANDO MODELO*/
module.exports = new Schema({
    uuid: {type: String},
    campaign_id: {type: Schema.ObjectId},
    hospital_id: {type: Schema.ObjectId},
    doctor_id: {type: Schema.ObjectId},
    identificacao: {type: String},
    nome: {type: String},
    idade: {type: Number},
    sexo: {type: Number},
    foto_tipo: {type: Number},
    exposicao_sol: {type: Number},
    historico_ca_pele: {type: Number},
    historico_familia_ca_pele: {type: Number},
    pessoa_risco_ca_pele: {type: Number},
    como_soube_campanha: [{type: Number}],
    diagnostico_clinico: [{type: Number}],
    evelocao: {type: Number},
    localizacao: [{type: Number}],
    conduta: [{type: Number}],

    campanha:{type:String},
    hospital:{type:String},
    medico:{type:String},

    status: {type: Number, required: true, default: status.ACTIVE},
    created_at: {type: Date, default: null},
    update_at: {type: Date, default: null},
    deleted_at: {type: Date, default: null},
    synced_at: {type: Date, default: null},
}, {collection: 'quiz'});
