/**
 * Descrição: AUTH GUARD, RESPONSAVEL POR AUTENTICAR O TOKEN DO USUARIO A CADA CONSULTA
 * Author: Thiago Silva
 */

/*IMPORTANDO BIBLIOTECAS*/
const jwt = require('jsonwebtoken');
const Service = require('../services/user');
const settings = require('../core/settings');
const message = require('../core/message');

/*METODO AUTH GUARDE PARA VALIDAR O TOKEN*/
const auth = async (req, res, next) => {
    // let teste = req as Request;
    

    let public = ['/api/campaign/list', '/api/hospital', '/api/quiz/reporting', '/api/quiz/import', '/api/quiz/export']
    if (!public.some(r => req.originalUrl.startsWith(r))) {
        try {
            const token = req.header('Authorization').replace('Bearer ', '');
            const data = jwt.verify(token, settings.JWT_KEY, {
                expiresIn: 500
            });
            const user = await Service.findOne({
                _id: data._id,
                'tokens.token': token
            });
            if (!user) {
                req.method = 'USER';
                res.status(404);
                return false;
            }
            req.user = user;
            req.token = token;
        } catch (e) {
            res.status(401);
            req.method = 'TOKEN';
            message.error(req, res, {})
        }
    }


    next();
};

/*EXPORTANDO MODULO AUTH*/
module.exports = auth;