/**
 * Descrição: SERVIÇO DE ENVIO DE E-MAIL
 * Author: Thiago Silva
 */

/*IMPORTANDO BIBLIOTECAS*/
const nodemailer = require('nodemailer');
const settings = require('./settings');
const inlineBase64 = require('nodemailer-plugin-inline-base64');
var mandrill = require('node-mandrill')(settings.API_KEY_MADRILL);

let email = {};

email.send = (to, subject, html, attachment = null) => {
    return new Promise((resolve, reject) => {

        let message = {
            to: to,
            from_email: settings.FROM,
            subject: subject,
            html: html,
            global_merge_vars: [{
                "name": "merge1",
                "content": "merge1 content"
            }]
        }

        if (attachment !== null) {
            message.attachments = [attachment]
        }

        mandrill('/messages/send', {
                message
            },
            (error, response) => {
                if (error) reject(JSON.stringify(error));
                else resolve(response);
            }
        );
    });
};

module.exports = email;