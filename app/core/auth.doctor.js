/**
 * Descrição: AUTH GUARD, RESPONSAVEL POR AUTENTICAR O TOKEN DO USUARIO A CADA CONSULTA
 * Author: Thiago Silva
 */

/*IMPORTANDO BIBLIOTECAS*/
const jwt = require('jsonwebtoken');
const Service = require('../services/doctor');
const settings = require('../core/settings');
const message = require('../core/message');

/*METODO AUTH GUARDE PARA VALIDAR O TOKEN*/
const authDoctor = async (req, res, next) => {

    console.log(req.originalUrl)
    let public = ['/api/quiz/report-error-sync', '/api/quiz/report-doctor', '/api/quiz/report-hospital']
    if (!public.some(r => req.originalUrl.startsWith(r))) {
        try {
            const token = req.header('Authorization').replace('Bearer ', '');
            const data = jwt.verify(token, settings.JWT_KEY_APP, {
                expiresIn: 500
            });
            const user = await Service.findOne({
                _id: data._id,
                'tokens.token': token
            });
            if (!user) {
                req.method = 'AUTH_DOCTOR';
                res.status(404);
                return false;
            }
            req.user = user;
            req.token = token;
        } catch (e) {
            res.status(401);
            req.method = 'TOKEN';
            message.error(req, res, {})
        }
    }

    next();
};

/*EXPORTANDO MODULO AUTH*/
module.exports = authDoctor;