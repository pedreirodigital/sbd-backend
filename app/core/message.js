/**
 * Descrição: ARQUIVO CODIGOS DE MESSAGERIA
 * Author: Thiago Silva
 */

/*DEFININDO OBJETO PRINCIPAL*/
const message = {};

/*MENSAGENS DE SUCESSO*/
message.POST_200 = `Registro cadastro com sucesso!`;
message.DELETE_200 = `Registro deletado com sucesso!`;
message.PUT_200 = `Registro editado com sucesso!`;
message.GET_200 = `Registros carregados com sucesso!`;
message.MAIL_200 = `O e-mail foi enviado com sucesso para o(s) rementente(s)!`;
message.UPLOAD_200 = `Upload realizado com sucesso!`;
message.DOCTOR_UPDATE_200 = `Seu cadastro já existe em nossos registos, foi adicionado a ele este novo posto de atendimento!`;
message.DOCTOR_UPDATE_DATA_200 = `Seus dados foram atualziados com sucesso!`;
message.RECOVER_DOCTOR_200 = `Você receberá em seu e-mail uma nova senha para acessar sua conta!`;
message.SERVICE_PUT_200 = `Posto de atendimento adicionado com sucesso!`;
message.CAMPAING_200 = `Campanha carregada com sucesso!`;

/*MENSAGENS DE ERROR*/
message.POST_400 = 'Não foi possivel salvar os dados deste registro no momento.\nverifique todos os campos e tente novamente!';
message.DELETE_400 = 'Não foi possivel deletar o registro no momento!';
message.PUT_400 = 'Não foi possivel atualizar os dados do registro no momento!';
message.GET_400 = 'Não há registros no momento!';
message.MAIL_400 = 'Não foi possivel enviar este email no momento!';
message.EXISTS_400 = 'Não foi possivel realizar este cadastro pois já foi encontrado o mesmo em nosso sistema!';
message.NO_MAIL_400 = 'O posto de atendimento não tem e-mail cadastrado para enviar o código!';
message.HAVE_DOCTOR_400 = 'Não é permitido excluir um posto de atendimento pois existe médicos vinculados a ele!';
message.DELETE_USER_400 = 'Não é possível excluir este usuário pois ele tem nivel supremo!';
message.USER_400 = 'Este usuário não foi encontrado ou não existe!';
message.AUTH_DOCTOR_400 = 'Este usuário não foi encontrado ou não existe!';
message.TOKEN_400 = 'Token expirado ou não é mais válido tente novamente!';
message.LOGIN_400 = 'Dados inválidos tente novamente!';
message.LOAD_IMAGE_400 = 'Não foi possivel carregar o arquivo solicitado!';
message.DOCTOR_400 = 'Já foi cadastrado um médico com este e-mail! ou CRM, para adicionar um novo posto de atendimento entre com seu login e adicione na seção postos de atendimento dentro do APP.';
message.NO_CODE_400 = 'O código informado não pertence a nenhum posto de atendimento tente novamente ou entre em contato com o posto de atendimento!';
message.RECOVER_DOCTOR_400 = `Não foi possível enviar seu e-mail de recuperação de senha tente novamente mais tarde novamente!`;
message.DOCTOR_NOT_FOUND_400 = `Não foi encontrado nenhum médico com o e-mail informado!`;
message.SERVICE_NOT_FOUND_400 = `Não foi encontrado nemhum posto de atendimento com esse código!`;
message.SERVICE_EXISTS_400 = `Este posto de atendimento já foi adicionado!`;
message.CAMPAING_NOTFOUND_400 = `Não foi possivel encontrar a campanha solicitada!`;
message.CAMPAING_400 = `Não foi possivel encontrar a campanha solicitada!`;

/*TRATAMENTO MENSAGENS SUCCESSO*/
message.show = (req, res, data) => {
    res.status(200).json(
        {
            status: 200,
            timer: 2000,
            code: `${req.method}_200`,
            message: message[`${req.method}_200`],
            data: data
        }
    );
};

/*TRAMENTO MENSAGEM DE ERROR*/
message.error = (req, res, error) => {
    let msg = message[`${req.method}_400`];
    let code = `${req.method}_400`;
    if (error.code) {
        msg = error.errmsg;
        code = error.code;
    }
    res.status(400).json(
        {
            status: 400,
            code: code,
            message: msg,
            error: error
        }
    );
};

/*EXPORTANDO O OBJECTO MESSAGE*/
module.exports = message;
