/**
 * Descrição: ARQUIVO DE CONEXÃO COM O MONGO
 * Author: Thiago Silva
 */

/*IMPORTANDO BIBLIOTECA DO MONGO*/
const mongoose = require('mongoose');
const settings = require('./settings');

/*CONEXÃO*/
mongoose.connect(settings.URL_DATABASE, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
});

module.exports = mongoose;
