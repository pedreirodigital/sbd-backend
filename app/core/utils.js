/**
 * Descrição: ARQUIVO COM AS CONFIGURAÇÕES PRINCIPAIS
 * Author: Thiago Silva
 */

const bcrypt = require('bcrypt');

/**
 *
 */
/*DEFININDO OBJETO PRINCIPAL*/
const utils = {};
utils.hash = null;
utils.hash_valide = null;

/**
 *
 * @param limit
 * @returns {string}
 */
utils.createNewPassword = (limit) => {
    return Math.random().toString(36).slice(limit);
};

/**
 *
 * @param password
 * @returns {*}
 */
utils.cryptPassword = (password) => {
    return bcrypt.hash(password, 10);
};

/**
 *
 * @param passowrd
 * @param hash
 * @returns {*}
 */
utils.checkPassword = async (passowrd, hash) => {
    return bcrypt.compare(passowrd, hash);
};

/**
 *
 * @param length
 * @returns {string}
 */
utils.createCode = (length) => {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (let i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text.toUpperCase();
};

module.exports = utils;
