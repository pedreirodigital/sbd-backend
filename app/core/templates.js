/**
 * Descrição: ARQUIVO COM AS CONFIGURAÇÕES PRINCIPAIS
 * Author: Thiago Silva
 */

const settings = require('./settings');
const Service = require("../services/campaign");

/*DEFININDO OBJETO PRINCIPAL*/
const templates = {};

/*CRIANDO CHAVE DE PARA CRIPTOGRAFIA DO JWT*/
templates.recover = (data) => {
    return '<body style="background-color: #f6f6f6;"></body><div style="width: 600px; margin-left: auto; color:#1b9886; margin-right: auto;  padding: 20px;">' +
        '<div style="border: #1b9886 solid 2px; box-sizing: border-box; display: inline-block; font-size: 14px; vertical-align: top; margin: 0; padding: 30px; border-radius: 7px; background-color: #fff;">' +
        '<img data-src="' + settings.LOGO + '" width="25%" style="margin-right: auto; margin-left: auto;"/>' +
        '<div style="padding: 20px;">' +
        '<h2>RECUPERAÇÃO DE SENHA</h2>' +
        '<div style="border-bottom:#1b9886 solid 1px; padding-bottom: 15px; margin-bottom: 15px;">Olá, <b>' + data.name + '</b> você solicitou uma recuperação de senha. Segue abaixo sua nova senha</div>' +
        '<h2 style="text-align: center;">Nova senha: ' + data.password + '<h2/>' +
        '</div>' +
        '</div>' +
        '<small style="text-align: center; display: block; padding: 15px; color:#6c757d;">Caso você não tenha solicitado esta recuperação por favor desconsidere este e-mail!</small>' +
        '</div></body>';
};

templates.recover_doctor = async (data) => {
    let campaing = await Service.find({_id: data.campaing});
    campaing = campaing[0];
    return '<body style="background-color: #f6f6f6;"></body><div style="width: 600px; margin-left: auto; color:' + campaing.theme.primary + '; margin-right: auto;  padding: 20px;">' +
        '<div style="border: ' + campaing.theme.primary + ' solid 2px; box-sizing: border-box; display: inline-block; font-size: 14px; vertical-align: top; margin: 0; padding: 30px; border-radius: 7px; background-color: #fff;">' +
        '<img data-src="' + campaing.logo + '" width="25%" style="margin-right: auto; margin-left: auto;"/>' +
        '<div style="padding: 20px;">' +
        '<h2>RECUPERAÇÃO DE SENHA</h2>' +
        '<div style="border-bottom:' + campaing.theme.primary + ' solid 1px; padding-bottom: 15px; margin-bottom: 15px;">Olá, <b>' + data.name + '</b> você solicitou uma recuperação de senha. Segue abaixo sua nova senha</div>' +
        '<h2 style="text-align: center;">Nova senha: ' + data.password + '<h2/>' +
        '</div>' +
        '</div>' +
        '<small style="text-align: center; display: block; padding: 15px; color:#6c757d;">Caso você não tenha solicitado esta recuperação por favor desconsidere este e-mail!</small>' +
        '</div></body>';
};

/*ENVIO DE CODIGO PARA O HOSPITAL*/
templates.sendCode = async (data) => {
    let campaing = await Service.find();
    campaing = campaing[0];
    return `<body style="background-color: #f6f6f6;"></body><div style="width: 600px; margin-left: auto; color:${campaing.theme.primary}; margin-right: auto;  padding: 20px;">
        <div style="border: ${campaing.theme.primary} solid 2px; box-sizing: border-box; display: inline-block; font-size: 14px; vertical-align: top; margin: 0; padding: 30px; border-radius: 7px; background-color: #fff;">
        <div style="padding: 20px;">
        <div style="border-bottom: ${campaing.theme.primary} solid 1px; padding-bottom: 15px; margin-bottom: 15px;">
        Prezado Coordenador <b>${data.name}</b>,</br><br>
        Os dados do atendimento da Campanha de Prevenção ao Câncer da Pele deverão ser preenchidos, através do aplicativo CapeleSBD, já disponibilizado nas lojas playstore (android) e apple store (ios).<br/><br/>
        A utilização do aplicativo, além de tornar a campanha mais sustentável trará uma agilidade necessária a divulgação dos dados.<br/><br/>
        O aplicativo contempla exatamente as mesmas perguntas que constavam no bloco de atendimento enviado nos anos anteriores para todos os postos.<br/><br/>
        Cada posto de atendimento possui um Código para registro. O Código abaixo informado deve ser enviado, com antecedência, para todos os médicos que trabalharão no posto sob sua coordenação no dia 07/12. Esse código será solicitado no primeiro acesso do médico ao aplicativo.<br/>
        <h2>CÓDIGO: ${data.code}</h2>
        Importante mencionar, que o aplicativo funciona off-line. No entanto, ele precisa ser baixado pelo médico voluntário antes do dia do atendimento e em local com internet disponível. Se no posto não há sinal de internet, o voluntário precisa se logar em local com sinal de internet antes de entrar no local de  atendimento. Todos os dados poderão ser preenchidos off-line. Assim, que terminar os atendimentos do dia, o médico deverá se dirigir a um local com Internet e com o aplicativo aberto para a transmissão dos dados.<br/><br/>
        Abaixo segue um link com passo a passo e orientações/esclarecimentos sobre como usar o aplicativo.<br/><br/>
        <a href="https://www.itarget.com.br/downloads/instrucoescapeleSBD.pdf">https://www.itarget.com.br/downloads/instrucoescapeleSBD.pdf</a><br/><br/>
        Qualquer dúvida, na utilização do aplicativo entrar em Contato pelo telefone ou whatsapp no número (85) 98183-0503.<br/><br/>
        Contamos com a colaboração de todos.<br/><br/>
        Atenciosamente,
         <br/><br/>
        Diretoria SBD Gestão 2019/2020<br/>
        </div>
        </div>
        </div>
        <small style="text-align: center; display: block; padding: 15px; color:#6c757d;">Esta mensagem e automática não precisa responde-lá!</small>
        </div></body>`;
};

/*EXPORTANDO O OBJECTO SETTINGS*/
module.exports = templates;
