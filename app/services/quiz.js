/**
 * Descrição: SERVIÇO QUESTIONARIO
 * Author: Thiago Silva
 */

const Model = require('../models/quiz');
const mongoose = require('mongoose');

/*ESTE METODO E CHAMANDO ANTES DE SALVAR OS DADOS NO BANCO*/
Model.pre('save', async function (next) {
    next();
});

/*EXPORTANDO O MODELO*/
module.exports = mongoose.model('Quiz', Model);
