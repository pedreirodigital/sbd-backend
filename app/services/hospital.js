/**
 * Descrição: SERVIÇO HOSPITAL
 * Author: Thiago Silva
 */

const Model = require('../models/hospital');
const settings = require('../core/settings');
const utils = require('../core/utils');
const mongoose = require('mongoose');

/*ESTE METODO E CHAMANDO ANTES DE SALVAR OS DADOS NO BANCO*/
Model.pre('save', async function (next) {
    this.code = utils.createCode(6);
    next();
});

/*EXPORTANDO O MODELO*/
module.exports = mongoose.model('Hospital', Model);
