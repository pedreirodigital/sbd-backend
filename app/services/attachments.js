/**
 * Descrição: SERVIÇO USUARIO
 * Author: Thiago Silva
 */

const Model = require('../models/attachments');
const mongoose = require('mongoose');

/*EXPORTANDO O MODELO*/
module.exports = mongoose.model('Attachments', Model);
