/**
 * Descrição: SERVIÇO USUARIO
 * Author: Thiago Silva
 */

const Model = require('../models/login');
const jwt = require('jsonwebtoken');
const settings = require('../core/settings');
const utils = require('../core/utils');
const mongoose = require('mongoose');

/*METODO PARA GERAR O TOKEN DE AUTENTICACAO*/
Model.methods.generateAuthToken = async function () {
    const token = jwt.sign({_id: this._id, logged: new Date()}, settings.JWT_KEY);
    this.tokens = this.tokens.concat({token});
    await this.save();
    return token;
};

/*METODO PARA GERAR NOVA SENHA*/
Model.methods.generatePassword = async function () {
    this.recover = String(utils.createNewPassword(4));
    await this.save();
    return this.recover;
};

/*METODO PARA CAPTURA AS CRENDECIAIS DO USUARIO*/
Model.statics.findByCredentials = async function (_email, password) {
    let result = await this.find({email: _email});

    if (result.length === 0)
        return false;

    if (result.length !== 0)
        result = result[0];

    if (result.recover !== null) {
        if (password === result.recover) {
            let hash = await utils.cryptPassword(result.recover);
            result.password = hash;
            result.recover = null;
            result.updated_recover_at = new Date();
        }
        await result.save();
    }

    let check = await utils.checkPassword(password, result.password).then(x => {
        return x;
    });

    if (!check) {
        return false;
    }

    return result;
};

/*CONSULTAR USUARIO POR E-MAIL*/
Model.statics.findByEmail = async function (email) {
    const result = await this.findOne({email});
    if (!result)
        return false;
    return {name, email} = result;
};

/*EXPORTANDO O MODELO*/
module.exports = mongoose.model('Login', Model);
