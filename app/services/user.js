/**
 * Descrição: SERVIÇO USUARIO
 * Author: Thiago Silva
 */

const Model = require('../models/user');
const utils = require('../core/utils');
const mongoose = require('mongoose');

Model.pre("save", async function (next, done) {
    this.password = await utils.cryptPassword(this.password);
    next();
});

/*EXPORTANDO O MODELO*/
module.exports = mongoose.model('User', Model);
