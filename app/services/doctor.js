/**
 * Descrição: SERVIÇO MÉDICO
 * Author: Thiago Silva
 */

const Model = require('../models/doctor');
const utils = require('../core/utils');
const jwt = require('jsonwebtoken');
const settings = require('../core/settings');
const mongoose = require('mongoose');

/*ESTE METODO E CHAMANDO ANTES DE SALVAR OS DADOS NO BANCO*/
Model.pre('save', async function (next) {
    if (this.password !== '' || this.password !== null && this.recover === null) {
        //this.password = await utils.cryptPassword(this.password);
    }
    next();
});

/*METODO PARA GERAR O TOKEN DE AUTENTICACAO*/
Model.methods.generateAuthTokenDoctor = async function () {
    const token = jwt.sign({_id: this._id, logged: new Date()}, settings.JWT_KEY_APP);
    this.tokens = this.tokens.concat({token});
    this.save();
    return token;
};

/*CONSULTAR MEDICOS POR HOSPITAL*/
Model.statics.getByDoctorForHospital = async function (id) {
    const result = await this.find({hospitais: id});
    if (!result)
        return false;
    return result;
};

/*CONSULTAR USUARIO POR E-MAIL*/
Model.statics.doctorExists = async function (data) {
    const result = await this.find().or([{crm: data.crm}, {email: data.email}]);
    if (!result)
        return false;
    return result;
};

/*METODO PARA GERAR NOVA SENHA*/
Model.methods.generatePasswordDoctor = async function () {
    this.recover = String(utils.createNewPassword(4));
    await this.save();
    return this.recover;
};

/*CONSULTAR USUARIO POR E-MAIL*/
Model.statics.findByEmailDoctor = async function (email) {
    const result = await this.findOne({email});
    if (!result)
        return false;
    return {name, email} = result;
};

/*METODO PARA CAPTURA AS CRENDECIAIS DO USUARIO*/
Model.statics.findByCredentialsDoctor = async function (_email, password) {
    let result = await this.find({email: _email});

    if (result.length === 0)
        return false;

    if (result.length !== 0)
        result = result[0];

    if (result.recover !== null) {
        if (password === result.recover) {
            result.password = result.recover;
            result.recover = null;
            result.updated_recover_at = new Date();
        }
        await result.save();
    }

    let check = (password === result.password);
    if (!check) {
        return false;
    }

    return result;
};

/*EXPORTANDO O MODELO*/
module.exports = mongoose.model('Doctor', Model);
