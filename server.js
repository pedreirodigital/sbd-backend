/**
 * Descrição: Arquivo responsável por levantar o serviço do Node para poder
 * executar a aplicação e a API através do Express.
 * Author: Thiago Silva
 */

/*IMPORTANDO BIBLIOTECAS DO NODE*/
const express = require('express');
const app = express();
const router = express.Router();
const settings = require('./app/core/settings');
const cors = require('cors');
require('./app/core/database');

app.use(express.static('public'));
app.use(express.static('uploads'));

app.use(express.json({
    limit: '50mb'
}));
app.use(express.urlencoded({
    limit: '50mb',
    extended: true
}));

app.use(cors());

/*DEFININDO FORMARO DO CORPO DA REQUISIÇÃO COMO JSON*/
app.use(express.json());

/* Middleware para usar em todos os requests enviados para a nossa API- Mensagem Padrão */
router.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header("Access-Control-Allow-Headers", "Content-Type,X-Amz-Date,Authorization,X-Api-Key,Origin,Accept,Access-Control-Allow-Headers,Access-Control-Allow-Methods,Access-Control-Allow-Origin");
    res.header("Access-Control-Allow-Headers", "*");

    if (req.method == 'OPTIONS') {
        res.status(200).end();
        next();
    } else {
        next();
    }
});
/*DANDO PERMISSÃO DE ACESSO A PASTA PUBLIC*/
app.use('/', express.static('public'));

/*ROTA PRINCIPAL*/
router.get('', (req, res) => {
    res.json({
        version: '1.0',
        message: 'API SBD SOCIEDADE BRASILEIRA DE DERMATOLOGIA',
        author: 'Thiago Silva',
        modules: 'USER, DOCTOR, HOSPITAL, QUIZ'
    });
});

/*DEFININDO PREFIXO DA API*/
app.use('/api', router);

/*DEFININDO ROTAS DOS MODULOS*/
app.use('/api/login', require('./app/api/login'));
app.use('/api/user', require('./app/api/user'));
app.use('/api/hospital', require('./app/api/hospital'));
app.use('/api/doctor', require('./app/api/doctor'));
app.use('/api/quiz', require('./app/api/quiz'));
app.use('/api/attachments', require('./app/api/attachments'));
app.use('/api/campaign', require('./app/api/campaign'));

/*INICIANDO O SERVIÇO DA APLICAÇÃO*/
app.listen(settings.PORT);

console.log('Iniciando a aplicação na porta ' + settings.PORT);


// let teste = async () => {

//     quizSvc = require('./app/services/quiz')
//     // count = await quizSvc.count()

//     teste = await quizSvc.aggregate(
//         [{
//                 "$lookup": {
//                     "from": "hospital",
//                     "localField": "hospital_id",
//                     "foreignField": "_id",
//                     "as": "hospital"
//                 }
//             },
//             {
//                 "$unwind": "$hospital"
//             }
//         ]
//     ).limit(1).sort({created_at: 1})
//     console.log(JSON.stringify(teste))
// }
// teste()